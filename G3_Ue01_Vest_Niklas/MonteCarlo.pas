(* MonteCarlo:              Vest Niklas, 17.03.2018 *)
(* -------                                          *)
(* Berechnet Volumen eines Rotationsellipsoids.     *)
(*==================================================*)
PROGRAM MonteCarlo;

USES Math;

TYPE
  Point3D = RECORD
    x, 
    y, 
    z: REAL;
  END;

  Ellipsoid = RECORD
    focus1, 
    focus2: Point3D;
    majorAxis, 
    minorAxis: REAL;
  END;

  (* Used for bounding box calculations *)
  Cuboid = RECORD
    startX,
    startY,
    startZ,
    deltaX,
    deltaY,
    deltaZ,
    volume: REAL;
  END;

const M = 32768;

(* Logs Ellipsoid details to the std output. *)
PROCEDURE WriteEllipse(ell: Ellipsoid);
BEGIN
  WITH ell DO BEGIN
    WriteLn('a: ', majorAxis/2:2:8);
    WriteLn('b & c: ', minorAxis/2:2:8);
    WriteLn('MajA: ', majorAxis:2:5);
    WriteLn('MinA: ', minorAxis:2:5);
    WriteLn('F1: x', focus1.x:2:5, ' y', focus1.y:2:5, ' z', focus1.z:2:5);
    WriteLn('F2: x', focus2.x:2:5, ' y', focus2.y:2:5, ' z', focus2.z:2:5);
  END;
END;

VAR
  (* "I am an ugly global variable and *)
  (* want to be capsulated within a class" *)
  x: LONGINT;

(* Custom pseudo-random-number generator *)
FUNCTION IntRand: INTEGER;
  CONST
    a = 3421;
    c = 1;
BEGIN
  x := (a * x + c) MOD M;
  IntRand := x;
END; 
FUNCTION RealRand: REAL;
BEGIN
  RealRand := IntRand / m;
END;

(* Returns the distance between to points in 3-dimensional space *)
FUNCTION DistanceBetween(p1, p2: Point3D): REAL;
VAR
  xDist, yDist, zDist: REAL;
BEGIN
  xDist := p2.x - p1.x;
  yDist := p2.y - p1.y;
  zDist := p2.z - p1.z;
  DistanceBetween := Sqrt(xDist*xDist + yDist*yDist + zDist*zDist);
END;

(* Relocates a point to the origin of the coordinate system. *)
PROCEDURE ToOrigin(VAR p: Point3D);
BEGIN
  p.x := 0;
  p.y := 0;
  p.z := 0;
END;

(* Moves an ellipsoid so that it's centered in the origin *)
(* of the coordinate system. *)
PROCEDURE Center(VAR ell: Ellipsoid);
VAR
  distCenter: REAL;
BEGIN
  WITH ell DO BEGIN
    distCenter := DistanceBetween(focus1, focus2)/2;

    (* move both focuses to the origin *)
    ToOrigin(focus1);
    ToOrigin(focus2);

    (* then stretch the ellipsoid to match *)
    (* the distance between the foci along *)
    (* the x-axis *)
    focus1.x := -distCenter;
    focus2.x := distCenter;
  END;
END;

(* Generates a bounding box for a specified ellipsoid. *)
(* The passed-in ellipsoid must be centered! *)
FUNCTION BoundingBoxOf(ell: Ellipsoid): Cuboid;
VAR
  boundingBox: Cuboid;
BEGIN
  WITH boundingBox DO BEGIN
    startX := -ell.majorAxis/2;
    startY := -ell.minorAxis/2;
    startZ := -ell.minorAxis/2;

    deltaX := ell.majorAxis;
    deltaY := ell.minorAxis;
    deltaZ := ell.minorAxis;

    volume := deltaX*deltaY*deltaZ;
  END;
  BoundingBoxOf := boundingBox;
END;

(* Calculates the minor axis for an Ellipsoid instance using mathemagic. *)
PROCEDURE calculateMinorAxis(VAR ell: Ellipsoid);
VAR
  cathete, hypothenuse: REAL;
BEGIN
  cathete := DistanceBetween(ell.focus1, ell.focus2)/2;
  hypothenuse := ell.majorAxis/2;
  ell.minorAxis := Sqrt(hypothenuse*hypothenuse - cathete*cathete)*2;
END;

(* Returns true if the Point3D {p} is within the Ellipsoid {ell}'s boundaries. *)
FUNCTION pointIsInEllipsoid(p: Point3D; ell: Ellipsoid): BOOLEAN;
BEGIN
  pointIsInEllipsoid := DistanceBetween(p, ell.focus1) + 
    DistanceBetween(p, ell.focus2) <= ell.majorAxis;
END;

(* Generates a Point3D within the specified Cuboid's bounds. *)
FUNCTION generatePointWithin(c: Cuboid): Point3D;
VAR
  p: Point3D;
BEGIN
  p.x := RealRand * c.deltaX + c.startX;
  p.y := RealRand * c.deltaY + c.startY;
  p.z := RealRand * c.deltaZ + c.startZ;

  generatePointWithin := p;
END;

(* Approximates the volume of an ellipsoid defined by two *)
(* focuses and a distance sum using random number generators. *)
FUNCTION Volume(f1, f2: Point3D; distSum: REAL): REAL;
const POINTS: LONGINT = 100000000;
VAR
  ell: Ellipsoid;
  ellBox: Cuboid;
  genPoint: Point3D;
  i, hits: LONGINT;
BEGIN
  (* Init RNG *)
  Randomize;
  x := 1;

  hits := 0;

  (* Construct Ellipsoid compound *)
  ell.focus1 := f1;
  ell.focus2 := f2;
  ell.majorAxis := distSum;
  calculateMinorAxis(ell);
  Center(ell);

  WriteEllipse(ell);

  (* Calculate bounding box for RNG *)
  ellBox := BoundingBoxOf(ell);

  FOR i := 1 TO POINTS DO BEGIN
    genPoint := generatePointWithin(ellBox);
    if pointIsInEllipsoid(genPoint, ell) then
      hits += 1;
  END;

  IF hits = 0 THEN Volume := 0
  ELSE Volume := (hits/POINTS) * ellBox.volume;
END;

VAR
  f1, f2: Point3D;
  vol: REAL;
  
BEGIN
  f1.x := 1.0;
  f1.y := 1.0;
  f1.z := 1.0;
  f2 := f1;

  vol := Volume(f1, f2, 2.0);
  WriteLn('Volume of Ellipsoid 1: ', vol:3:8);

  f2.x := 3.0;
  f2.y := 5.0;
  f2.z := 1.0;
  
  (* WHICH DIST SUM? 420? 1080? *)
  vol := Volume(f1,f2, 10.0);
  WriteLn('Volume of Ellipsoid 2: ', vol:3:8);
END.