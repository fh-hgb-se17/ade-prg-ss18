(* ArrayInspector:          Vest Niklas, 17.03.2018 *)
(* -------                                          *)
(* Sammelt Daten über die Folge      einer Sequenz  *)
(* von Pseudozufallszahlen.                         *)
(*==================================================*)
PROGRAM ArrayInspector;

CONST MAX = 20;

TYPE
  IntArray = ARRAY[1..MAX] OF INTEGER;

(* Returns true if the tested pair of numbers   *)
(* does not match the specified sequence "mode" *)
FUNCTION RunDisrupted(shouldAscend: BOOLEAN; VAR arr: IntArray;
                      currentIndex: INTEGER) : BOOLEAN;
VAR
  nowDescending, nowAscending: BOOLEAN;
BEGIN
  nowDescending := shouldAscend AND (arr[currentIndex] < arr[currentIndex-1]);
  nowAscending := (NOT shouldAscend) AND (arr[currentIndex] > arr[currentIndex-1]);
  RunDisrupted := nowDescending OR nowAscending;
END;

PROCEDURE UpdateStats(shouldAscend: BOOLEAN;
                      length: INTEGER;
                      VAR ascArr, descArr: IntArray;
                      VAR maxAsc, maxDesc: INTEGER);
BEGIN 
  IF shouldAscend THEN BEGIN
    ascArr[length] += 1;
    IF length > maxAsc THEN 
      maxAsc := length;
  END 
  ELSE BEGIN
    descArr[length] += 1;
    IF (length) > maxDesc THEN 
      maxDesc := length;
  END;
END;

PROCEDURE ComputeRuns(n: INTEGER;
                         VAR maxAsc, maxDesc: INTEGER;
                         VAR asc, desc: IntArray);
VAR
  generatedArray: IntArray;
  i, runStart: INTEGER;
  currentlyAscending: BOOLEAN;
BEGIN

  runStart := 1;
  currentlyAscending := false;

  maxAsc := 1;
  maxDesc := 1;

  (* prepare counter arrays *)
  FOR i := 1 TO n DO BEGIN 
    asc[i] := 0;
    desc[i] := 0;
  END;

  Randomize;
  FOR i := 1 TO n DO BEGIN

    (* Initialize array with random numbers *)
    generatedArray[i] := Random(100);
    WriteLn('[',i,'] ', generatedArray[i], ' ');

    (* decide whether the new run is ascending or descending *)
    IF (i - runStart = 1) THEN BEGIN
      currentlyAscending := generatedArray[i] > generatedArray[i-1];
    END
    ELSE IF i > 1 THEN BEGIN
      IF RunDisrupted(currentlyAscending, generatedArray, i) THEN BEGIN
        (* increment counter and max values if the run was disrupted *)
        UpdateStats(currentlyAscending, i-runStart, asc, desc, maxAsc, maxDesc);
        (* new run starts from here! *)
        runStart := i;
      END; (* IF RunDisrupted *)
    END;

    IF ((i = n) AND (i - runStart <> 0)) THEN BEGIN
      UpdateStats(generatedArray[i] > generatedArray[i-1], 
        i-runStart+1, asc, desc, maxAsc, maxDesc);
    END;
  END;
  
  WriteLn;
END;

VAR
  i, n, maxAsc, maxDesc: INTEGER;
  asc_arr, desc_arr: IntArray;

BEGIN
  n := 10;
  ComputeRuns(n, maxAsc, maxDesc, asc_arr, desc_arr);

  WriteLn('Max Ascending: ', maxAsc);
  WriteLn('Max Descending: ', maxDesc);

  WriteLn('Nr. of ascending runs: ');
  FOR i := 1 TO maxAsc DO WriteLn('Ascending runs of length ', i, ': ', asc_arr[i]);

  WriteLn('Nr. of descending runs: ');
  FOR i := 1 TO maxDesc DO WriteLn('Descending runs of length ', i, ': ', desc_arr[i]);
END.