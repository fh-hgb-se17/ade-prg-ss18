(* Wildcards:                            Vest Niklas, 15.04.2018 *)
(* -----------                                                   *)
(* A program for testing strings against wildcard patterns.      *)
(*===============================================================*)
PROGRAM Wildcards;

  CONST 
    MATCH_ANY_ONE = '?';
    MATCH_ANY_ONE_OR_MORE = '*';
    EOL = '$';

  FUNCTION Matching(pattern: String; pi: INTEGER; str: String; stri: INTEGER): BOOLEAN;
    VAR
      matches: BOOLEAN;
  BEGIN
    matches := FALSE;
    IF (pi < Length(pattern)) AND (stri < Length(str)) THEN BEGIN
      IF (pattern[pi] = str[stri]) OR (pattern[pi] = MATCH_ANY_ONE) THEN BEGIN
        (* Characters match exactly or pattern at pi matches any character *)
        matches := TRUE;
        Inc(pi);
        Inc(stri);
      END ELSE IF pattern[pi] = MATCH_ANY_ONE_OR_MORE THEN BEGIN
        IF pattern[pi+1] = str[stri] THEN BEGIN
          (* End of '*'' match *)
          pi := pi + 2;
          Inc(stri);
        END 
        ELSE BEGIN
          (* Still within '*' match *)
          Inc(stri);
        END;
        matches := TRUE;
      END;
      (* Now match remaining string *)
      matches := matches AND Matching(pattern, pi, str, stri);
    END
    ELSE IF (pi = Length(pattern)) AND (stri = Length(str)) THEN BEGIN
      (* End of the pattern AND the string where reached (match) *)
      matches := TRUE;
    END
    ELSE IF (stri = Length(str)) AND (pattern[pi] = MATCH_ANY_ONE_OR_MORE) AND (pattern[pi + 1] = EOL) THEN BEGIN
      (* The pattern ends with a * so it matches the rest of str *)
      matches := TRUE;
    END;

    Matching := matches;
  END;

  PROCEDURE Test(pattern, str: String);
  BEGIN 
    WriteLn('Testing for pattern ', pattern, ' in string ', str, '. Match: ', 
    Matching(pattern, 1, str, 1));
  END;
BEGIN
  Test('ABC$', 'ABC$');    (* matches *)
  Test('ABC$', 'AB$');
  Test('ABC$', 'ABCD$');
  Test('A?C$', 'AXC$');    (* matches *)
  Test('*$', '$');         (* matches *)
  Test('*$', 'HYHSGNF$');  (* matches *)
  Test('A*C$', 'AC$');     (* matches *)
  Test('A*C$', 'AXYZC$');  (* matches *)
END.