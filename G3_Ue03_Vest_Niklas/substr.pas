(* Substring:                            Vest Niklas, 15.04.2018 *)
(* -----------                                                   *)
(* A program for finding the longest common substring.           *)
(*===============================================================*)
PROGRAM Substring;

  (* Checks if s1[s1Offset] is the start of a substring found in s2 *)
  PROCEDURE FindLongestSubstring(s1, s2: String; s1Offset: INTEGER; 
                                 VAR l, r: INTEGER);
    VAR
      tl, tr, i, j: INTEGER;
  BEGIN
    l := 0;
    r := 0;
    tl := 0;
    tr := 0;

    i := 1;
    WHILE (i <= Length(s1)) AND (i <= Length(s2)) DO BEGIN
      j := 0;
      IF s1[s1Offset] = s2[i] THEN BEGIN
        (* Start of a new substring *)
        tl := i;
        WHILE s1[s1Offset + j] = s2[i + j] DO BEGIN
          (* Find end of the substring *)
          tr := i + j;
          Inc(j);
        END;
        IF tr - tl >= r - l THEN BEGIN
          (* If the new substring is longer, replace the old one*)
          l := tl;
          r := tr;
        END;
      END;
      i := i + j + 1;
    END;
  END;

  PROCEDURE FindLongestMatch(s1, s2: String; VAR ss: String;
                             VAR l1, r1, l2, r2: INTEGER);
    VAR
      tl2, tr2, i: INTEGER;
  BEGIN
    ss := '';
    l1 := 0;
    r1 := 0;
    l2 := 0;
    r2 := 0;

    i := 0;
    WHILE i <= Length(s1) DO BEGIN
      (* Check if the substring starting at i ind s1 *)
      (* Is a substring of s2 *)
      FindLongestSubstring(s1, s2, i, tl2, tr2);
      IF (tr2 - tl2 >= r1 - l1) AND (tl2 <> 0) THEN BEGIN
        (* If the new substring is longer, replace the old one *)
        l1 := i;
        r1 := i + (tr2 - tl2);
        l2 := tl2;
        r2 := tr2;
        (* jump past found string here *)
        i := i + (tr2 - tl2);
      END;
      Inc(i);
    END;
    IF l1 <> 0 THEN BEGIN
      ss := Copy(s1, l1, r1 - l1 + 1);
    END;
  END;

  VAR
    s1, s2, longest: String;
    l1, r1, l2, r2: INTEGER;
BEGIN
  l1 := 0;
  r1 := 0;
  l2 := 0;
  r2 := 0;
  longest := '';

  s1 := 'abcdefg';
  s2 := 'adeb';
  FindLongestMatch(s1, s2, longest, l1, r1, l2, r2);
  WriteLn('f(', s1, ', ', s2, ') = ', longest, ' (s1[', l1, ':', r1, '] s2[', l2, ':', r2, '])');

  s1 := 'uvwxy';
  s2 := 'rstqvwxz';
  FindLongestMatch(s1, s2, longest, l1, r1, l2, r2);
  WriteLn('f(', s1, ', ', s2, ') = ', longest, ' (s1[', l1, ':', r1, '] s2[', l2, ':', r2, '])');

  s1 := 'abcdefg';
  s2 := 'hijklmno';
  FindLongestMatch(s1, s2, longest, l1, r1, l2, r2);
  WriteLn('f(', s1, ', ', s2, ') = ', longest, ' (s1[', l1, ':', r1, '] s2[', l2, ':', r2, '])');

  s1 := 'abcdefg';
  s2 := 'hehe';
  FindLongestMatch(s1, s2, longest, l1, r1, l2, r2);
  WriteLn('f(', s1, ', ', s2, ') = ', longest, ' (s1[', l1, ':', r1, '] s2[', l2, ':', r2, '])');

  s1 := 'bcdbcdef';
  s2 := 'hambcdehum';
  FindLongestMatch(s1, s2, longest, l1, r1, l2, r2);
  WriteLn('f(', s1, ', ', s2, ') = ', longest, ' (s1[', l1, ':', r1, '] s2[', l2, ':', r2, '])');
END.