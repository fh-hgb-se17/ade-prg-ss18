(* AbstractSyntaxTree:                       Niklas Vest, 08.05.2018.2018 *)
(* -------                                                                *)
(* A Program which generates an abstract syntax tree from a specified     *)
(* line - follwing the rules for basic maths - and calculating the        *)
(* result.                                                                *)
(* ====================================================================== *)
PROGRAM AbstractSyntaxTreeParser;

  USES AbstractTree;

  CONST
    eosCh = Chr(0);

  TYPE
    SymbolCode = (noSy, (*error/wrong symbol*)
                   eosSy,
                   plusSy, minusSy, timesSy, divSy,
                   leftParSy, rightParSy,
                   number
                   );

  VAR
    line: STRING;

    ch: CHAR;        (*current character*)
    cnr: INTEGER;

    sy: SymbolCode;  (*current symbol*)
    numberSTr: STRING;

    syntaxTreeRoot: NodePtr;

    success: BOOLEAN;


  (* ===== Scanner ===== *)
  PROCEDURE NewCh;
  BEGIN
    IF cnr < Length(line) THEN BEGIN
      cnr := cnr + 1;
      ch := line[cnr];
    END
    ELSE BEGIN
      ch := eosCh;
    END;
  END; (*NewCh*)


  PROCEDURE NewSy;
  BEGIN
    WHILE ch = ' ' DO BEGIN  (*ignore spaces*)
      NewCh;
    END;

    CASE ch OF
      eosCh: BEGIN
        sy := eosSy;
      END;
      '+': BEGIN
        sy := plusSy; NewCh;
      END;
      '-': BEGIN
        sy := minusSy; NewCh;
      END;
      '*': BEGIN
        sy := timesSy; NewCh;
      END;
      '/': BEGIN
        sy := divSy; NewCh;
      END;
      '(': BEGIN
        sy := leftParSy; NewCh;
      END;
      ')': BEGIN
        sy := rightParSy; NewCh;
      END;
      '0'..'9': BEGIN
        sy := number;
        numberStr := ch;
        NewCh;
        WHILE (ch >= '0') AND (ch <= '9') DO BEGIN
          numberStr := numberStr + ch;
          NewCh;
        END; (*WHILE*)
      END;
      ELSE BEGIN
        sy := noSy; NewCh;
      END;
    END; (*CASE*)
  END; (*NewSy*)


  (* ====== PARSER ============ *)
  PROCEDURE Expr(VAR syntaxTree: NodePtr); FORWARD;
  PROCEDURE Term(VAR syntaxTree: NodePtr); FORWARD;
  PROCEDURE Fact(VAR syntaxTree: NodePtr); FORWARD;

  PROCEDURE S;
  BEGIN
    (* Create the root of the syntax tree *)
    syntaxTreeRoot := CreateTreeNode;
    Expr(syntaxTreeRoot); IF NOT success THEN Exit;
    IF sy <> eosSy THEN BEGIN
      success := FALSE;
      Exit
    END;
  END; (*S*)

  (*Expr  = Term { '+' Term | '-' Term } .*)
  PROCEDURE Expr(VAR syntaxTree: NodePtr);
    VAR newN: NodePtr;
  BEGIN
    Term(syntaxTree); IF NOT success THEN Exit;
    WHILE (sy = plusSy) OR (sy = minusSy) DO BEGIN
      (* Create a new parent node to store the current operation *)
      newN := CreateTreeNode;
      newN^.left := syntaxTree;
      newN^.right := CreateTreeNode;
      syntaxTree := newN;
      CASE sy OF
        plusSy: BEGIN
          syntaxTree^.val := AbstractTree.Plus;
          NewSy;
          (* Append the second operand in the right pointer *)
          Term(syntaxTree^.right); IF NOT success THEN Exit;
        END;
        minusSy: BEGIN
          syntaxTree^.val := AbstractTree.Minus;
          NewSy;
          Term(syntaxTree^.right); IF NOT success THEN Exit;
        END;
      END; (*CASE*)
    END; (*WHILE*)
  END; (*Expr*)

  (* Term = Fact { '*‘ Fact | '/' Fact } . *)
  PROCEDURE Term(VAR syntaxTree: NodePtr);
    VAR newN: NodePtr;
  BEGIN
    Fact(syntaxTree); IF NOT success THEN Exit;
    WHILE (sy = timesSy) OR (sy = divSy) DO BEGIN
      (* Same as in Expr *)
      newN := CreateTreeNode;
      newN^.left := syntaxTree;
      newN^.right := CreateTreeNode;
      syntaxTree := newN;
      CASE sy OF
        timesSy: BEGIN
          syntaxTree^.val := AbstractTree.Multiply;
          NewSy;
          Fact(syntaxTree^.right); IF NOT success THEN Exit;
        END;
        divSy:   BEGIN
          syntaxTree^.val := AbstractTree.Divide;
          NewSy;
          Fact(syntaxTree^.right); IF NOT success THEN Exit;
        END;
      END; (*CASE*)
    END; (*WHILE*)
  END; (*Term*)

  (*Fact  = number | '(' Expr ')' . *)
  PROCEDURE Fact(VAR syntaxTree: NodePtr);
  BEGIN
    CASE sy OF
      number: BEGIN
        (* --> the current node is a number *)
        syntaxTree^.val := numberStr;
        NewSy;
      END;
      leftParSy: BEGIN
        NewSy;
        (* There is another expression to be parsed *)
        (* Create a new expression-tree from this node *)
        Expr(syntaxTree); IF NOT success THEN Exit;
        IF sy <> rightParSy THEN BEGIN
          success := FALSE;
          Exit;
        END; (*IF*)
        NewSy;
      END;
    ELSE
      success := FALSE;
    END; (*CASE*)
  END; (*Fact*)

BEGIN
  (* Init *)
  line := '4/2+3-3*2';
  cnr := 0;
  NewCh;
  NewSy;
  WriteLn(line);

  (* Run *)
  success := TRUE;
  S;

  (* Results *)
  IF success THEN BEGIN
    PrintTree(syntaxTreeRoot);
    WriteLn(' = ', ValueOf(syntaxTreeRoot));
    DisposeTree(syntaxTreeRoot);
  END
  ELSE BEGIN
    WriteLn('Syntax error at ', cnr);
  END;
END. (*ExprPgm*)
