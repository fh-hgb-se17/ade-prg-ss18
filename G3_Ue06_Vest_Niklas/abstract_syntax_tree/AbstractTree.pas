(* AbstractTree:                             Niklas Vest, 08.05.2018.2018 *)
(* -------                                                                *)
(* A module for basic tree operations as well as a function to calculate  *)
(* the result of the tree given that it only contains numeric values and  *)
(* operators.                                                             *)
(* ====================================================================== *)
UNIT AbstractTree;

INTERFACE

  CONST
    Plus = '+';
    Minus = '-';
    Multiply = '*';
    Divide = '/';

  TYPE
    NodePtr = ^Node;
    Node = RECORD
      left, right: NodePtr;
      val: STRING[10]; (* 10 chars oughta be onough for everyone *)
    END;

  FUNCTION CreateTreeNode: NodePtr;

  FUNCTION ValueOf(n: NodePtr): INTEGER;

  PROCEDURE PrintTree(n: NodePtr);

  PROCEDURE DisposeTree(VAR n: NodePtr);

IMPLEMENTATION

  FUNCTION CreateNode(val: STRING): NodePtr;
  VAR
    n: NodePtr;
  BEGIN
    New(n);
    n^.val := val;
    n^.left := nil;
    n^.right := nil;
    CreateNode := n;
  END;

  FUNCTION CreateTreeNode: NodePtr;
  BEGIN
    CreateTreeNode := CreateNode('');
  END;

  FUNCTION ValueOf(n: NodePtr): INTEGER;
    VAR result, code: INTEGER;
  BEGIN
    IF n <> NIL THEN BEGIN
      (* Guaranteed to be a true binary tree *)
      (* --> only one pointer needs to be checked *)
      IF n^.left <> NIL THEN BEGIN
        CASE n^.val[1] OF
          Plus: ValueOf := ValueOf(n^.left) + ValueOf(n^.right);
          Minus: ValueOf := ValueOf(n^.left) - ValueOf(n^.right);
          Multiply: ValueOf := ValueOf(n^.left) * ValueOf(n^.right);
          Divide: ValueOf := ValueOf(n^.left) DIV ValueOf(n^.right);
        END;
      END
      ELSE BEGIN
        result := 0;
        Val(n^.val, result, code);
        ValueOf := result;
      END;
    END;
  END;

  PROCEDURE PrintTree(n: NodePtr);
  BEGIN
    IF n <> NIL THEN BEGIN
      IF n^.left <> NIL THEN Write('(');
      PrintTree(n^.left);
      Write(n^.val);
      PrintTree(n^.right);
      IF n^.left <> NIL THEN Write (')');
    END;
  END;

  PROCEDURE DisposeTree(VAR n: NodePtr);
  BEGIN
    IF n <> NIL THEN BEGIN
      DisposeTree(n^.left);
      DisposeTree(n^.right);
      Dispose(n);
      n := NIL;
    END;
  END;

END.
