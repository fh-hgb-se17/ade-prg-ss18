(* CanonicalSyntaxTree:                      Niklas Vest, 08.05.2018.2018 *)
(* -------                                                                *)
(* A Program which stores arithmetical expressions in a can. sytnax tree. *)
(* ====================================================================== *)
PROGRAM CanonicalSyntaxTree;

USES Tree;

CONST
  eosCh = Chr(0);

TYPE
  SymbolCode = (noSy, (*error/wrong symbol*)
                 eosSy,
                 plusSy, minusSy, timesSy, divSy,
                 leftParSy, rightParSy,
                 number
                 );

VAR
  line: STRING;

  ch: CHAR; (*current character*)
  cnr: INTEGER;

  sy: SymbolCode; (*current symbol*)
  numberStr: STRING;

  syntaxTreeNode: NodePtr;

  success: BOOLEAN;


  (* ===== Scanner ===== *)
  PROCEDURE NewCh;
  BEGIN
    IF cnr < Length(line) THEN BEGIN
      cnr := cnr + 1;
      ch := line[cnr];
    END
    ELSE BEGIN
      ch := eosCh;
    END;
  END; (*NewCh*)


  PROCEDURE NewSy;
  BEGIN
    WHILE ch = ' ' DO BEGIN
      (*ignore spaces*)
      NewCh;
    END;

    CASE ch OF
      eosCh: BEGIN
        sy := eosSy;
      END;
      '+': BEGIN
        sy := plusSy;
        NewCh;
      END;
      '-': BEGIN
        sy := minusSy;
        NewCh;
      END;
      '*': BEGIN
        sy := timesSy;
        NewCh;
      END;
      '/': BEGIN
        sy := divSy;
        NewCh;
      END;
      '(': BEGIN
        sy := leftParSy;
        NewCh;
      END;
      ')': BEGIN
        sy := rightParSy;
        NewCh;
      END;
      '0'..'9': BEGIN
        sy := number;
        numberStr := ch;
        NewCh;
        WHILE (ch >= '0') AND (ch <= '9') DO
        BEGIN
          numberStr := numberStr + ch;
          NewCh;
        END; (*WHILE*)
      END;
    ELSE
    BEGIN
      sy := noSy;
      NewCh;
    END;
    END; (*CASE*)
  END; (*NewSy*)


  (* ====== PARSER ============ *)
  PROCEDURE Expr(syntaxNode: NodePtr); FORWARD;
  PROCEDURE Term(syntaxNode: NodePtr); FORWARD;
  PROCEDURE Fact(syntaxNode: NodePtr); FORWARD;

  PROCEDURE S;
  BEGIN
    syntaxTreeNode := InitTree;
    Expr(syntaxTreeNode);
    IF NOT success THEN Exit;
    IF sy <> eosSy THEN BEGIN
      success := FALSE;
      Exit
    END;
  END; (*S*)

  (*Expr  = Term { '+' Term | '-' Term } .*)
  PROCEDURE Expr(syntaxNode: NodePtr);
  BEGIN
    syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Term);
    Term(syntaxNode);
    IF NOT success THEN Exit;
    WHILE (sy = plusSy) OR (sy = minusSy) DO
    BEGIN
      CASE sy OF
        plusSy: BEGIN
          syntaxNode := AppendSiblingToSyntaxTree(syntaxNode, Tree.Plus);
          NewSy;
          syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Term);
          Term(syntaxNode);
          IF NOT success THEN Exit;
        END;
        minusSy: BEGIN
          syntaxNode := AppendSiblingToSyntaxTree(syntaxNode, Tree.Minus);
          NewSy;
          syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Term);
          Term(syntaxNode);
          IF NOT success THEN Exit;
        END;
      END; (*CASE*)
    END; (*WHILE*)
  END; (*Expr*)

  (* Term = Fact { '*‘ Fact | '/' Fact } . *)
  PROCEDURE Term(syntaxNode: NodePtr);
  BEGIN
    syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Fact);
    Fact(syntaxNode);
    IF NOT success THEN Exit;
    WHILE (sy = timesSy) OR (sy = divSy) DO
    BEGIN
      CASE sy OF
        timesSy: BEGIN
          syntaxNode := AppendSiblingToSyntaxTree(syntaxNode, Tree.Multiply);
          NewSy;
          syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Fact);
          Fact(syntaxNode);
          IF NOT success THEN Exit;
        END;
        divSy: BEGIN
          syntaxNode := AppendSiblingToSyntaxTree(syntaxNode, Tree.Divide);
          NewSy;
          syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Fact);
          Fact(syntaxNode);
          IF NOT success THEN Exit;
        END;
      END; (*CASE*)
    END; (*WHILE*)
  END; (*Term*)

  (*Fact  = number | '(' Expr ')' . *)
  PROCEDURE Fact(syntaxNode: NodePtr);
  BEGIN
    CASE sy OF
      number: BEGIN
        ExtendSyntaxTreeBy(syntaxNode, numberStr);
        NewSy;
      END;
      leftParSy: BEGIN
        NewSy;
        syntaxNode := ExtendSyntaxTreeBy(syntaxNode, Tree.Expr);
        Expr(syntaxNode);
        IF NOT success THEN Exit;
        IF sy <> rightParSy THEN BEGIN
          success := FALSE;
          Exit;
        END; (*IF*)
        NewSy;
      END;
    ELSE
      success := FALSE;
    END; (*CASE*)
  END; (*Fact*)

BEGIN
  (* Init *)
  line := '(2+15)*3';
  cnr := 0;
  NewCh;
  NewSy;
  WriteLn(line);

  (* Run *)
  success := TRUE;
  S;

  (* Results *)
  IF success THEN BEGIN
    PrintSyntaxTree;
    DisposeSyntaxTree;
  END
  ELSE
    WriteLn('syntax error at ', cnr - 1);

END.
