(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT Point2DU;

INTERFACE

  TYPE
    Point2D = RECORD
      x, y: REAL;
    END;

IMPLEMENTATION

END.
