(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT DisplayableVectorU;

INTERFACE

  USES DisplayableU;

  TYPE
    DisplayableLambda = PROCEDURE (d: Displayable);
    DisplayableArr = ARRAY[1..1] OF Displayable;
    DisplayableVector = OBJECT
    PUBLIC // Methods
      CONSTRUCTOR Init(initialCapacity: INTEGER);
      DESTRUCTOR Done;

      PROCEDURE Add(d: Displayable); VIRTUAL;

      FUNCTION GetElementAt(pos: INTEGER): Displayable;

      PROCEDURE ForEach(f: DisplayableLambda);

      (* Returns nr. of elements in the Vector *)
      FUNCTION Size: INTEGER;

      (* Returns the nr. of elements that fit in *)
      (* the Vector before it has to reallocate  *)
      FUNCTION Capacity: INTEGER;

    PRIVATE // Members
      vecCapacity, vecSize: INTEGER;
      displayables: ^DisplayableArr;

    PRIVATE // Methods
      (* Increases capacity of the Vector by allocating more memory *)
      PROCEDURE Reallocate(newCapacity: INTEGER);

      (* Returns TRUE when the size of the Vector is equal *)
      (* to it's capacity                                  *)
      FUNCTION IsFull: BOOLEAN;
    END;

IMPLEMENTATION

    (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR DisplayableVector.Init(initialCapacity: INTEGER);
  BEGIN
    vecSize := 0;
    (* capacity is set in the call to Vec.Reallocate as well but        *)
    (* if the assignment was left out at this point, the constructor    *)
    (* would depend on the implementation of the reallocation procedure *)
    vecCapacity := initialCapacity;
    displayables := NIL;
    Reallocate(initialCapacity);
  END;

  PROCEDURE DisplayableVector.Add(d: Displayable);
  BEGIN
    IF IsFull THEN Reallocate(Capacity * 2);
    Inc(vecSize);
    (*$R-*) displayables^[Size] := val (*$R+*)
  END;

  FUNCTION DisplayableVector.GetElementAt(pos: INTEGER): Displayable;
  BEGIN
    IF (pos < 1) OR (pos > Size) THEN BEGIN
      WriteLn('Index ', pos, ' is out of bounds for vector of size ', Size);
      HALT;
    END
    ELSE
      GetElementAt := (*$R-*) displayables^[pos]; (*$R+*)
  END;

  PROCEDURE DisplayableVector.ForEach(f: DisplayableLambda);
    VAR
      current: Displayable;
      i: INTEGER;
  BEGIN
    FOR i := 1 TO Size DO
      f(GetElementAt(i))
  END;

  FUNCTION DisplayableVector.Capacity: INTEGER;
  BEGIN
    Capacity := vecCapacity;
  END;

  FUNCTION DisplayableVector.Size: INTEGER;
  BEGIN
    Size := vecSize;
  END;

  DESTRUCTOR DisplayableVector.Done;
  BEGIN
    FreeMem(displayables, SizeOf(INTEGER) * Capacity);
    displayables := NIL;
  END;

  FUNCTION DisplayableVector.Iterator: DisplayableVectorIterator;
    VAR dvi: DisplayableVectorIterator;
  BEGIN
    Iterator := New(dvi, Init(@SELF));
  END;

    (* ////////// PRIVATE METHODS ////////// *)

  PROCEDURE DisplayableVector.Reallocate(newCapacity: INTEGER);
  VAR
    newData: ^DisplayableArr;
    i: INTEGER;
  BEGIN
    GetMem(newData, SizeOf(INTEGER) * Capacity);

    (* skipped if size is 0 *)
    FOR i := 1 TO Size DO BEGIN (*$R-*)
      newData^[i] := displayables^[i];
    END; (*$R+*)
    FreeMem(displayables, SizeOf(INTEGER) * Capacity);
    vecCapacity := newCapacity;
    displayables := newData;
  END;

  FUNCTION DisplayableVector.IsFull: BOOLEAN;
  BEGIN
    IsFull := Size = Capacity;
  END;

END.

