(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT CircleU;

INTERFACE

  USES
    Point2DU,
    DisplayableU;

  TYPE
    Circle = OBJECT(Displayable)
      PUBLIC
        CONSTRUCTOR Init(position: Point2D; radius: REAL);
        DESTRUCTOR Done; VIRTUAL;
        PROCEDURE Draw; VIRTUAL;

      PROTECTED
        radius: REAL;
    END;

IMPLEMENTATION

  CONSTRUCTOR Circle.Init(position: Point2D; radius: REAL);
  BEGIN
    SELF.position := position;
    SELF.radius := radius;
  END;

  DESTRUCTOR Circle.Done;
  BEGIN
    INHERITED Done;
  END;

  PROCEDURE Circle.Draw;
  BEGIN
    // TODO
  END;

END.
