(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT LineU;

INTERFACE

  USES
    Point2DU,
    DisplayableU;

  TYPE
    Line = OBJECT(Displayable)
      PUBLIC
        CONSTRUCTOR Init(origin, destination: Point2D);
        DESTRUCTOR Done; VIRTUAL;
        PROCEDURE Draw; VIRTUAL;

      PROTECTED
        destination: Point2D;
    END;

IMPLEMENTATION

  CONSTRUCTOR Line.Init(origin, destination: Point2D);
  BEGIN
    position := origin;
    SELF.destination := destination;
  END;

  DESTRUCTOR Line.Done;
  BEGIN
    INHERITED Done;
  END;

  PROCEDURE Line.Draw;
  BEGIN
    // TODO
  END;

END.
