(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT DisplayableU;

INTERFACE

  USES Point2DU;

  TYPE
    Displayable = OBJECT
      PUBLIC
        CONSTRUCTOR Init(position: Point2D);
        DESTRUCTOR Done; VIRTUAL;
        PROCEDURE Draw; VIRTUAL; ABSTRACT;
        PROCEDURE MoveBy(delta: Point2D); VIRTUAL;

    PROTECTED
        position: Point2D;
    END;

IMPLEMENTATION

  CONSTRUCTOR Displayable.Init(position: Point2D);
  BEGIN
    SELF.position := position;
  END;

  DESTRUCTOR Displayable.Done;
  BEGIN
    (* Nothing to do *)
  END;

  PROCEDURE Displayable.MoveBy(delta: Point2D);
  BEGIN
    position.x := position.x + delta.x;
    position.y := position.y + delta.y;
  END;

END.
