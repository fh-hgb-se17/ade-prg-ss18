(*
 * Project: SpielbergCartoonStudio
 * User: Niklas Vest
 * Date: 05/06/2018
 *)
UNIT PictureU;

INTERFACE

  USES
    Point2DU,
    DisplayableU,
    DisplayableVectorU;

  TYPE
    Picture = OBJECT(Displayable)
      PUBLIC
        CONSTRUCTOR Init(position: Point2D);
        DESTRUCTOR Done; VIRTUAL;
        PROCEDURE Draw; VIRTUAL;
        PROCEDURE MoveBy(delta: Point2D);
        PROCEDURE AddDisplayable(d: Displayable);

      PROTECTED
        children: DisplayableVector;
    END;

IMPLEMENTATION

  CONSTRUCTOR Picture.Init(position: Point2D);
  BEGIN
    SELF.position := position;
    New(children.Init(5));
  END;

  DESTRUCTOR Picture.Done;
  BEGIN
    children.Done;
    INHERITED Done;
  END;

  PROCEDURE Picture.Draw;
    PROCEDURE DrawDisplayable(d: Displayable);
    BEGIN d.Draw; END;
  BEGIN
    children.ForEach(DrawDisplayable);
  END;

  PROCEDURE Picture.MoveBy(delta: Point2D);
    PROCEDURE MoveDisplayable(d: Displayable);
    BEGIN d.MoveBy(delta) END;
  BEGIN
    children.ForEach(MoveDisplayable);
  END;

  PROCEDURE Picture.AddDisplayable(d: Displayable);
  BEGIN
    children.Add(d);
  END;

END.
