(* HashGraph:                                     Niklas Vest, 02.04.2018 *)
(* -------                                                                *)
(* Programm for visualizing hash function mapping distribution.           *)
(* ====================================================================== *)
PROGRAM HashGraph;

USES
{$IFDEF FPC}
  Windows,
{$ELSE}
  WinTypes, WinProcs,
{$ENDIF}
  Strings, 
  WinCrt,
  Math,
  WinGraph,
  WordReader,
  WordHashTableCh;

VAR
  entries: ARRAY[1..WordHashTableCh.TABLE_SIZE] OF INTEGER;
  maxColls: INTEGER;

PROCEDURE Redraw(dc: HDC; wnd: HWnd; r: TRect); FAR;
VAR
  i, curr: INTEGER;
  xDivisions, yDivisions: REAL;
BEGIN
  xDivisions := r.width / WordHashTableCh.TABLE_SIZE;
  yDivisions := r.height / maxColls;
  FOR i := 1 TO WordHashTableCh.TABLE_SIZE DO BEGIN
    curr := Round(i * xDivisions);
    MoveTo(dc, curr, r.height);
    LineTo(dc, curr, r.height - Round(entries[i] * yDivisions));
  END;
END;

VAR
  w: Word;
  i: INTEGER;

BEGIN
  WriteLn('WordCounter:');
  OpenFile('tests/KafkaWords.txt', toLower);
  ReadWord(w);

  WHILE w <> '' DO BEGIN
    IncreaseCounterFor(w);
    ReadWord(w);
  END;
  CloseFile;

  (* calculate collisions once so redraw event *)
  (* does not iterate over hashtable *)
  maxColls := 0;
  FOR i := 1 TO WordHashTableCh.TABLE_SIZE DO BEGIN 
    entries[i] := WordHashTableCh.EntriesAt(i);
    maxColls := Max(entries[i], maxColls);
  END;

  redrawProc := Redraw;
  WGMain;
END.
