(* HashGraph:                                     Niklas Vest, 02.04.2018 *)
(* -------                                                                *)
(* Unit for creating a hash table.                                        *)
(* ====================================================================== *)
UNIT WordHashTableCh;

INTERFACE

USES
  WordReader;

CONST
  (* Is public so HashGraph can use it *)
  TABLE_SIZE = 503;

TYPE
  ListNodePtr = ^ListNode;
  ListNode = RECORD
    w: Word;
    occurrences: INTEGER;
    next: ListNodePtr;
  END;

(* Inserts a given word to the hash table *)
PROCEDURE IncreaseCounterFor(w: Word);

(* Returns the list node containing the most used word *)
FUNCTION MostUsedWordPtr: ListNodePtr;

(* Returns the amount of keys that map to this index *)
FUNCTION EntriesAt(ind: INTEGER): INTEGER;

(* Clears all data associated with the unit *)
PROCEDURE DisposeTable;

IMPLEMENTATION

CONST
  MAX_KEY_VAL = Ord(High(CHAR)) + 1;

TYPE 
  HashFunction = FUNCTION (key: Word): INTEGER;

VAR
  _words: ARRAY[1..TABLE_SIZE] OF ListNodePtr;
  _hf: HashFunction;

FUNCTION _hash(key: Word): INTEGER;
VAR
  h, i: INTEGER;
BEGIN
  h := 0;
  FOR i := 1 TO Length(key) DO BEGIN 
    (*$Q-*)
    (*$R-*)
    h := 31 * h + Ord(key[i]);
    (*$R+*)
    (*$Q+*)
  END;
  _hash := Abs(h) MOD TABLE_SIZE + 1;
END;

FUNCTION _hash1ForWinGraph(key: WORD): INTEGER;
VAR
  h, i: INTEGER;
BEGIN
  h := 0;
  FOR i := 1 TO Length(key) DO
    h += Ord(key[i]) + i;
  _hash1ForWinGraph := h MOD TABLE_SIZE + 1;
END;

FUNCTION _hash2ForWinGraph(key: WORD): INTEGER;
VAR
  h: INTEGER;
BEGIN
  h := Ord(key[1]) * Length(key);
  _hash2ForWinGraph := h MOD TABLE_SIZE + 1;
END;

(* Creates a new node with the specified word and *)
(* the occurrences counter set to 1 *)
FUNCTION _createNode(w: Word): ListNodePtr;
VAR
  n: ListNodePtr;
BEGIN
  New(n);
  n^.w := w;
  n^.occurrences := 1;
  n^.next := NIL;
  _createNode := n;
END;

(* Appends a passed node to the specified list *)
PROCEDURE _appendNode(VAR l: ListNodePtr; n: ListNodePtr);
VAR
  curr: ListNodePtr;
BEGIN
  IF l <> NIL THEN BEGIN
    curr := l;
    WHILE curr^.next <> NIL DO 
      curr := curr^.next;
    curr^.next := n;
  END ELSE l := n;
END;

(* Returns the node holding the word with a high number of *)
(* occurrences. If a and b are NIL, returns NIL! *)
FUNCTION _max(a, b: ListNodePtr): ListNodePtr;
VAR
  maxNode: ListNodePtr;
BEGIN 
  IF (a <> NIL) AND (b <> NIL) THEN BEGIN 
    IF a^.occurrences > b^.occurrences THEN
      maxNode := a
    ELSE
      maxNode := b;
  END ELSE IF a <> NIL THEN
    maxNode := a
  ELSE
    maxNode := b;
  _max := maxNode;
END;

(* Returns the node holding the word with the highest number *)
(* of occurrences in the specified list. *)
FUNCTION _maxInList(l: ListNodePtr): ListNodePtr;
VAR
  maxNode: ListNodePtr;
BEGIN
  maxNode := l;
  IF (l <> NIL) THEN BEGIN
    l := l^.next;
    WHILE l <> NIL DO BEGIN
      maxNode := _max(l, maxNode);
      l := l^.next;
    END;
  END;
  _maxInList := maxNode;
END;

(* Deletes a list *)
PROCEDURE _disposeList(VAR l: ListNodePtr);
VAR
  curr, pred: ListNodePtr;
BEGIN 
  IF l <> NIL THEN BEGIN
    curr := l;
    WHILE curr <> NIL DO BEGIN 
      pred := curr;
      curr := curr^.next;
      Dispose(pred);
    END;
    l := NIL;
  END;
END;

(* Could be optimized by inserting in a sorted manner and *)
(* only searching for pred^.w < w before appending *)
PROCEDURE IncreaseCounterFor(w: Word);
VAR
  ind: INTEGER;
  l, pred: ListNodePtr;
BEGIN
  ind := _hf(w);
  pred := _words[ind];
  
  IF pred = NIL THEN 
  	(* If there hasn't been a key that maps to the same hash as w *)
    _words[ind] := _createNode(w)
  ELSE BEGIN 
  	(* Else -> check if the key w was already inserted *)
    l := pred^.next;
    WHILE (l <> NIL) AND (pred^.w <> w) DO BEGIN
      pred := l;
      l := l^.next;
    END;
    (* If w exists, increase occurrence counter, else append as new node *)
    IF pred^.w = w THEN
      pred^.occurrences += 1
    ELSE 
      _appendNode(_words[ind], _createNode(w));
  END;
END;

FUNCTION MostUsedWordPtr: ListNodePtr;
VAR
  i: INTEGER;
  totalMax: ListNodePtr;
BEGIN
  totalMax := NIL;
  FOR i := Low(_words) TO TABLE_SIZE DO
  	(* check every occurrence counter against curren totalMax *)
    totalMax := _max(_maxInList(_words[i]), totalMax);
  MostUsedWordPtr := totalMax;
END;

FUNCTION EntriesAt(ind: INTEGER): INTEGER;
VAR
  entries: INTEGER;
  n: ListNodePtr;
BEGIN 
  entries := 0;
  n := _words[ind];
  WHILE n <> NIL DO BEGIN 
    entries += 1;
    n := n^.next;
  END;
  EntriesAt := entries;
END;

PROCEDURE DisposeTable;
VAR
  i: INTEGER;
BEGIN
  FOR i := Low(_words) TO TABLE_SIZE DO
    _disposeList(_words[i]);
END;

BEGIN 
  WriteLn('Loading hash table unit (chaining).');
  _hf := _hash;
END.