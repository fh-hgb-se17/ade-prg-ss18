(* WordCounter:                          Vest Niklas, 02.04.2018 *)
(* -----------                                                   *)
(* Reads Words into a specified data structure (binary tree or   *)
(* hash table) and prints the most used word within a text file. *)
(*===============================================================*)
PROGRAM WordCounter;

USES
  WinCrt, 
  Timer, 
  WordReader, 
  BinaryWordTree,
  WordHashTableCh,
  WordHashTableOA;

TYPE
  (* Simple enum to configure the data structure to *)
  (* use when reading in words from a file *)
  DataStructure = (BinaryTree, HashTableCh, HashTableOA);

VAR
  w: Word;
  n: LONGINT;
  ds: DataStructure;
  tree: TreeNodePtr;

PROCEDURE WriteStats(w: Word; occurrences: INTEGER);
BEGIN 
  WriteLn('Most used word: ', w);
  WriteLn('Counter: ', occurrences);
END;

PROCEDURE PrintTreeResults(tree: TreeNodePtr);
VAR
  maxNode: TreeNodePtr;
BEGIN 
  IF tree <> NIL THEN BEGIN
    maxNode := BinaryWordTree.MostUsedWordPtr(tree);
    WriteStats(maxNode^.w, maxNode^.occurrences);
    BinaryWordTree.DisposeTree(tree);
  END;
END;

PROCEDURE PrintHashTableChResults;
VAR
  maxNode: ListNodePtr;
BEGIN 
  maxNode := WordHashTableCh.MostUsedWordPtr;
  IF maxNode <> NIL THEN BEGIN 
    WriteStats(maxNode^.w, maxNode^.occurrences);
    WordHashTableCh.DisposeTable;
  END;
END;

PROCEDURE PrintHashTableOAResults;
VAR
  maxNode: DataPtr;
BEGIN 
  maxNode := WordHashTableOA.MostUsedWordPtr;
  IF maxNode <> NIL THEN BEGIN 
    WriteStats(maxNode^.w, maxNode^.occurrences);
    WordHashTableOA.DisposeTable;
  END;
END;

BEGIN (*WordCounter*)
  ds := HashTableCh;
  tree := NIL;
  WriteLn('WordCounter:');
  OpenFile('tests/kafka.txt', toLower);
  StartTimer;
  n := 0;
  ReadWord(w);
  WHILE w <> '' DO BEGIN
    n := n + 1;

    CASE ds OF
      HashTableCh: WordHashTableCh.IncreaseCounterFor(w);
      HashTableOa: WordHashTableOA.IncreaseCounterFor(w);
      BinaryTree: BinaryWordTree.IncreaseCounterFor(tree, w);
    END;

    ReadWord(w);
  END; (*WHILE*)
  StopTimer;
  CloseFile;
  WriteLn('number of words: ', n);
  WriteLn('elapsed time:    ', ElapsedTime);

  CASE ds OF
    HashTableCh: PrintHashTableChResults;
    HashTableOA: PrintHashTableOAResults;
    BinaryTree: PrintTreeResults(tree);
  END;
END. (*WordCounter*)