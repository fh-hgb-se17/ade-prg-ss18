UNIT WordHashTableOA;

INTERFACE

USES
  WordReader;

TYPE
  DataPtr = ^Data;
  Data = RECORD
    w: Word;
    occurrences: INTEGER;
  END;

(* Inserts a given word to the hash table *)
PROCEDURE IncreaseCounterFor(w: Word);

(* Returns the list node containing the most used word *)
FUNCTION MostUsedWordPtr: DataPtr;

(* Clears all data associated with the unit *)
PROCEDURE DisposeTable;

IMPLEMENTATION

CONST
  TABLE_SIZE = 10001;
  MAX_KEY_VAL = Ord(High(CHAR)) + 1;

VAR
  _words: ARRAY[1..TABLE_SIZE] OF DataPtr;

FUNCTION _hash(key: Word): INTEGER;
VAR
  h, i: INTEGER;
BEGIN
  h := 0;
  FOR i := 1 TO Length(key) DO BEGIN 
    (*$Q-*)
    (*$R-*)
    h := 31 * h + Ord(key[i]);
    (*$R+*)
    (*$Q+*)
  END;
  _hash := Abs(h) MOD TABLE_SIZE + 1;
END;

(* Creates a new node with the specified word and *)
(* the occurrences counter set to 1 *)
FUNCTION _createNode(w: Word): DataPtr;
VAR
  ptr: DataPtr;
BEGIN
  New(ptr);
  ptr^.w := w;
  ptr^.occurrences := 1;
  _createNode := ptr;
END;

(* Returns the node holding the word with a high number of *)
(* occurrences. If a and b are NIL, returns NIL! *)
FUNCTION _max(a, b: DataPtr): DataPtr;
VAR
  maxNode: DataPtr;
BEGIN 
  IF (a <> NIL) AND (b <> NIL) THEN BEGIN 
    IF a^.occurrences > b^.occurrences THEN
      maxNode := a
    ELSE
      maxNode := b;
  END ELSE IF a <> NIL THEN
    maxNode := a
  ELSE
    maxNode := b;
  _max := maxNode;
END;

PROCEDURE IncreaseCounterFor(w: Word);
VAR
  ind: INTEGER;
BEGIN
  ind := _hash(w);

  (* Search for a new home for w to live in *)
  WHILE (ind <= TABLE_SIZE) AND (_words[ind] <> NIL) AND (_words[ind]^.w <> w) DO 
    ind += 1;

  IF ind <= TABLE_SIZE THEN BEGIN
    IF _words[ind] = NIL THEN
      _words[ind] := _createNode(w) 
    ELSE IF _words[ind]^.w = w THEN
      _words[ind]^.occurrences += 1;
  END;
  (* At this point we should probably look from *)
  (* Low(_words) to _hash(w) - 1 *)
END;

FUNCTION MostUsedWordPtr: DataPtr;
VAR
  i: INTEGER;
  totalMax: DataPtr;
BEGIN
  totalMax := NIL;
  FOR i := Low(_words) TO TABLE_SIZE DO
    totalMax := _max(_words[i], totalMax);
  MostUsedWordPtr := totalMax;
END;

PROCEDURE DisposeTable;
VAR
  i: INTEGER;
BEGIN
  FOR i := Low(_words) TO TABLE_SIZE DO BEGIN
    Dispose(_words[i]);
    _words[i] := NIL;
  END;
END;

BEGIN 
  WriteLn('Loading hash table unit (open adressing).');
END.