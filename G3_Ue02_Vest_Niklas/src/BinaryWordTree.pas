(* BinaryWordTree:                       Vest Niklas, 02.04.2018 *)
(* -----------                                                   *)
(* A BinaryTree unit for storing words along with their          *)
(* occurrences within a certain text.                            *)
(*===============================================================*)
UNIT BinaryWordTree;

INTERFACE

USES WordReader;

TYPE
  TreeNodePtr = ^TreeNode;
  TreeNode = RECORD
    w: Word;
    occurrences: INTEGER;
    left, right: TreeNodePtr;
  END;

(* Inserts a given word to the bernareh treh *)
PROCEDURE IncreaseCounterFor(VAR tree: TreeNodePtr; VAR s: Word);

(* Returns the tree node containing the most used word *)
FUNCTION MostUsedWordPtr(VAR tree: TreeNodePtr): TreeNodePtr;

(* Prints the whole tree pre-order *)
PROCEDURE Write(tree: TreeNodePtr);

(* Guess what this boi does *)
PROCEDURE DisposeTree(VAR tree: TreeNodePtr);

IMPLEMENTATION

(* Returns a pointer to a new tree node with occurrences set to 0 *)
FUNCTION _createNode(s: Word): TreeNodePtr;
VAR
  n: TreeNodePtr;
BEGIN 
  New(n);
  n^.left := NIL;
  n^.right := NIL;
  n^.w := s;
  n^.occurrences := 0;
  _createNode := n;
END;

(* Returns a tree node for which the value of w matches s *)
FUNCTION _nodeFor(VAR tree: TreeNodePtr; VAR s: Word): TreeNodePtr;
VAR
  n: TreeNodePtr;
BEGIN
  n := NIL;
  IF tree <> NIL THEN BEGIN
    IF s > tree^.w THEN
      n := _nodeFor(tree^.right, s)
    ELSE IF s < tree^.w THEN
      n := _nodeFor(tree^.left, s)
    ELSE
      n := tree;
  END ELSE BEGIN
    n := _createNode(s);
    tree := n;
  END;

  _nodeFor := n;
END;

(* Preconidtion: a and b <> nil *)
FUNCTION _max(a, b: TreeNodePtr): TreeNodePtr;
VAR
  n: TreeNodePtr;
BEGIN 
  n := NIL;
  IF a^.occurrences < b^.occurrences THEN
    n := b
  ELSE
    n := a;
  _max := n;
END;

PROCEDURE IncreaseCounterFor(VAR tree: TreeNodePtr; VAR s: Word);
VAR
  n: TreeNodePtr;
BEGIN
  n := _nodeFor(tree, s);
  n^.occurrences += 1;
END;

FUNCTION MostUsedWordPtr(VAR tree: TreeNodePtr): TreeNodePtr;
  VAR
  n, leftMax, rightMax: TreeNodePtr;
BEGIN
  n := NIL;
  IF tree <> NIL THEN BEGIN
    (* recursively get max of left and right *)
    leftMax := MostUsedWordPtr(tree^.left);
    rightMax := MostUsedWordPtr(tree^.right);

    (* if both <> nil, get max by occurrences *)
    IF (leftMax <> NIL) AND (rightMax <> NIL) THEN 
      n := _max(leftMax, rightMax)
    ELSE IF leftMax <> NIL THEN (* else just use existing one *)
      n := leftMax
    ELSE IF rightMax <> NIL THEN
      n := rightMax;

    IF n = NIL THEN (* If there are no subtrees *)
      n := tree
    ELSE (* else see if tree or the "sub-max" has more occurrences *)
      n := _max(tree, n);
  END;
  MostUsedWordPtr := n;
END;

PROCEDURE Write(tree: TreeNodePtr);
BEGIN 
  IF tree <> NIL THEN BEGIN
    Write(tree^.left);
    WriteLn(tree^.w, ': ', tree^.occurrences);
    Write(tree^.right);
  END;
END;

PROCEDURE DisposeTree(VAR tree: TreeNodePtr);
BEGIN
  IF tree <> NIL THEN BEGIN
    DisposeTree(tree^.left);
    DisposeTree(tree^.right);
    Dispose(tree);
  END;
END;

BEGIN 
  WriteLn('Loading binary tree unit.');
END.