UNIT Vector;

INTERFACE

  TYPE
    IntArr = ARRAY[1..1] OF INTEGER;
    Vec = OBJECT
      PUBLIC // Methods
        CONSTRUCTOR Init(initialCapacity: INTEGER);
        DESTRUCTOR Done;

        (* Appends value to the back of the Vector *)
        PROCEDURE Add(val: INTEGER); VIRTUAL;

        (* Inserts the supplied element at the specified position, *)
        (* shifting all elements from <pos> to <size> to the right *)
        PROCEDURE InsertElementAt(pos: INTEGER; val: INTEGER); VIRTUAL;

        (* Returns the value at the specified position in the Vector *)
        (* if the index is valid, otherwise <ok> is set to false     *)
        PROCEDURE GetElementAt(pos: INTEGER; VAR val: INTEGER; VAR ok: BOOLEAN);

        (* Returns nr. of elements in the Vector *)
        FUNCTION Size: INTEGER;

        (* Returns the nr. of elements that fit in *)
        (* the Vector before it has to reallocate  *)
        FUNCTION Capacity: INTEGER;

        (* Set's the size back to 0, leaving the capacity as is *)
        PROCEDURE Clear;

      PRIVATE // Members
        vecCapacity, vecSize: INTEGER;
        data: ^IntArr;

      PRIVATE // Methods
        (* Increases capacity of the Vector by allocating more memory *)
        PROCEDURE Reallocate(newCapacity: INTEGER);

        (* Returns TRUE when the size of the Vector is equal *)
        (* to it's capacity                                  *)
        FUNCTION IsFull: BOOLEAN;
    END;

IMPLEMENTATION

  (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR Vec.Init(initialCapacity: INTEGER);
  BEGIN
    vecSize := 0;
    (* capacity is set in the call to Vec.Reallocate as well but        *)
    (* if the assignment was left out at this point, the constructor    *)
    (* would depend on the implementation of the reallocation procedure *)
    vecCapacity := initialCapacity;
    data := NIL;
    Reallocate(initialCapacity);
  END;

  PROCEDURE Vec.Add(val: INTEGER);
  BEGIN
    IF IsFull THEN Reallocate(Capacity * 2);
    Inc(vecSize);
    (*$R-*) data^[Size] := val (*$R+*)
  END;

  PROCEDURE Vec.InsertElementAt(pos: INTEGER; val: INTEGER);
    VAR i: INTEGER;
  BEGIN
    IF IsFull THEN Reallocate(Capacity * 2);    (* Double the capacity *)
    IF pos < 1 THEN InsertElementAt(1, val)     (* Add to front if < 1 *)
    ELSE IF pos > Size THEN Add(val)            (* Add to back if > size *)
    ELSE BEGIN  (*$R-*)                         (* else shift other elements right *)
      FOR i := Size DOWNTO pos DO BEGIN
        data^[i+1] := data^[i];
      END;
      data^[pos] := val;
      Inc(vecSize);
    END;(*$R+*)
  END;

  PROCEDURE Vec.GetElementAt(pos: INTEGER; VAR val: INTEGER; VAR ok: BOOLEAN);
  BEGIN
    IF (pos < 1) OR (pos > Size) THEN
      ok := FALSE
    ELSE BEGIN
      val := (*$R-*) data^[pos]; (*$R+*)
      ok := TRUE;
    END;
  END;

  FUNCTION Vec.Capacity: INTEGER;
  BEGIN
    Capacity := vecCapacity;
  END;

  FUNCTION Vec.Size: INTEGER;
  BEGIN
    Size := vecSize;
  END;

  PROCEDURE Vec.Clear;
  BEGIN
    vecSize := 0;
  END;

  DESTRUCTOR Vec.Done;
  BEGIN
    FreeMem(data, SizeOf(INTEGER) * Capacity);
    data := NIL;
  END;

  (* ////////// PRIVATE METHODS ////////// *)

  PROCEDURE Vec.Reallocate(newCapacity: INTEGER);
    VAR
      newData: ^IntArr;
      i: INTEGER;
  BEGIN
    GetMem(newData, SizeOf(INTEGER) * Capacity);

    (* skipped if size is 0 *)
    FOR i := 1 TO Size DO BEGIN (*$R-*)
      newData^[i] := data^[i];
    END; (*$R+*)
    FreeMem(data, SizeOf(INTEGER) * Capacity);
    vecCapacity := newCapacity;
    data := newData;
  END;

  FUNCTION Vec.IsFull: BOOLEAN;
  BEGIN
    IsFull := Size = Capacity;
  END;

END.
