UNIT PrimeVector;

INTERFACE

  USES Vector;

  TYPE
    PrimeVec = OBJECT(Vec)
      CONSTRUCTOR Init(initialCapacity: INTEGER);
      DESTRUCTOR Done;

      (* Adds a value to the back of the vector *)
      (* only if it is a prime number           *)
      PROCEDURE Add(val: INTEGER); VIRTUAL;

      (* Inserts a value to the vector only if it is a prime  *)
      (* number. For more information see Vec.InsertElementAt *)
      PROCEDURE InsertElementAt(pos: INTEGER; val: INTEGER); VIRTUAL;
    END;

IMPLEMENTATION

  (* ////////// PRIVATE FUNCTIONS ////////// *)

  (* Wikipedias 6k += 1 optimization pseudo code port *)
  FUNCTION IsPrime(val: INTEGER): BOOLEAN;
    VAR
      i: INTEGER;
      prime: BOOLEAN;
  BEGIN
    prime := TRUE;
    IF val <= 1 THEN
      prime := FALSE
    ELSE IF val <= 3 THEN
      prime := TRUE
    ELSE IF (val MOD 2 = 0) OR (val MOD 3 = 0) THEN
      prime := FALSE
    ELSE BEGIN
      i := 5;
      WHILE (i * i <= val) AND prime DO BEGIN
        IF (val MOD i = 0) OR (val mod (i + 2) = 0) THEN
          prime := FALSE;
        i := i + 6;
      END;
    END;
    IsPrime := prime;
  END;

  (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR PrimeVec.Init(initialCapacity: INTEGER);
  BEGIN
    INHERITED Init(initialCapacity);
  END;

  PROCEDURE PrimeVec.Add(val: INTEGER);
  BEGIN
    IF IsPrime(val) THEN
      INHERITED Add(val);
  END;

  PROCEDURE PrimeVec.InsertElementAt(pos: INTEGER; val: INTEGER);
  BEGIN
    IF IsPrime(val) THEN
      INHERITED InsertElementAt(pos, val);
  END;

  DESTRUCTOR PrimeVec.Done;
  BEGIN
    INHERITED Done;
  END;

END.
