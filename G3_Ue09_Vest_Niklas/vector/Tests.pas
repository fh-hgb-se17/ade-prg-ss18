PROGRAM Tests;

  USES
    Vector,
    NaturalVector,
    PrimeVector,
    Assertions;

  VAR
    v: Vec;
    nv: NaturalVec;
    pv: PrimeVec;
    ok: BOOLEAN;
    tmp: INTEGER;

BEGIN

  WriteTestSectionName('vector');
  v.Init(5);

  (* Tests: Add, Size, Capacity(, Reallocate) *)
  v.Add(3);
  v.Add(9);
  v.Add(14);
  v.Add(-2);
  v.Add(8);
  AssertEquals(5, v.Size, 'Correct size 1');
  AssertEquals(5, v.Capacity, 'Correct capacity 1');

  v.Add(-100);
  AssertEquals(6, v.Size, 'Correct size 2');
  AssertEquals(10, v.Capacity, 'Correct capacity 2');

  (* Tests: GetElementAt *)
  v.GetElementAt(3, tmp, ok);
  AssertTrue(ok, 'Using valid indices');
  AssertEquals(14, tmp, 'Fetching element by using valid indices');
  v.GetElementAt(0, tmp, ok);
  AssertFalse(ok, 'Using invalid indices (index < 1)');
  v.GetElementAt(9, tmp, ok);
  AssertFalse(ok, 'Using invalid indices (index > size)');

  (* Tests: InsertElementAt *)
  v.InsertElementAt(-13, 99);
  v.GetElementAt(1, tmp, ok);
  AssertEquals(99, tmp, 'Appending to front by using an index < 1');
  v.InsertElementAt(1000, 2);
  v.GetElementAt(v.Size, tmp, ok);
  AssertEquals(2, tmp, 'Appending to back by using an index > Size');
  v.InsertElementAt(3, -100);
  v.GetElementAt(3, tmp, ok);
  AssertEquals(-100, tmp, 'Inserting in the middle by using a valid index');

  (* Tests: Clear *)
  v.Clear;
  AssertEquals(0, v.Size, 'Correct size after clear');

  v.Done;

  WriteTestSectionName('naturalvector');
  nv.Init(3);

  (* Tests: Add *)
  nv.Add(10);
  nv.Add(-1);
  nv.Add(0);
  AssertEquals(1, nv.Size, 'Adding non-positive numbers');

  (* Tests: InsertElementAt *)
  nv.InsertElementAt(1, 99);
  nv.GetElementAt(1, tmp, ok);
  AssertEquals(99, tmp, 'Inserting elements to NaturalVec');

  nv.Done;

  WriteTestSectionName('primevector');
  pv.Init(3);

  (* Tests: Add *)
  pv.add(31);
  pv.add(2);
  pv.add(-1);
  pv.add(15);
  AssertEquals(2, pv.Size, 'Adding non-prime numbers');

  (* Tests: InsertElementAt *)
  pv.InsertElementAt(2, 7);
  pv.GetElementAt(2, tmp, ok);
  AssertEquals(7, tmp, 'Inserting elements to PrimeVec');

  pv.Done;
END.
