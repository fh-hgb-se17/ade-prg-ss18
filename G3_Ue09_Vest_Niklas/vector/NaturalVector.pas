UNIT NaturalVector;

INTERFACE

  USES Vector;

  TYPE
    NaturalVec = OBJECT(Vec)
      CONSTRUCTOR Init(initialCapacity: INTEGER);
      DESTRUCTOR Done;

      (* Adds a value to the back of the vector *)
      (* only if it is a natural number         *)
      PROCEDURE Add(val: INTEGER); VIRTUAL;

      (* Inserts a value to the vector only if it is a natural *)
      (* number. For more information see Vec.InsertElementAt  *)
      PROCEDURE InsertElementAt(pos: INTEGER; val: INTEGER); VIRTUAL;
    END;

IMPLEMENTATION

  (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR NaturalVec.Init(initialCapacity: INTEGER);
  BEGIN
    INHERITED Init(initialCapacity);
  END;

  PROCEDURE NaturalVec.Add(val: INTEGER);
  BEGIN
    IF val > 0 THEN
      INHERITED Add(val);
  END;

  PROCEDURE NaturalVec.InsertElementAt(pos: INTEGER; val: INTEGER);
  BEGIN
    IF val > 0 THEN
      INHERITED InsertElementAt(pos, val);
  END;

  DESTRUCTOR NaturalVec.Done;
  BEGIN
    INHERITED Done;
  END;

END.
