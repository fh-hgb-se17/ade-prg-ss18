PROGRAM Tests;

  USES
    ListType,
    SortedListType,
    Assertions;

  VAR
    li: List;
    sli: SortedList;

BEGIN

  WriteTestSectionName('list');
  li.Init;

  (* Tests: Add, Size, Contains *)
  li.Add(3);
  li.Add(8);
  li.add(3);
  li.Add(-10);
  li.add(3);
  li.add(3);
  AssertEquals(6, li.Size, 'Correct size');
  AssertTrue(li.Contains(3), 'Contains predicate (with previously added value)');
  AssertFalse(li.Contains(-3), 'Contains predicate (with value not previously added)');

  (* Tests: Remove *)
  li.Remove(3);
  AssertFalse(li.Contains(3), 'Value removal');
  AssertEquals(2, li.Size, 'Correct size after removal');

  (* Tests: Clear *)
  li.Clear;
  AssertEquals(0, li.Size, 'Correct size after clear');

  li.Done;

  WriteTestSectionName('sortedlist');
  sli.Init;

  (* Tests: Add *)
  sli.Add(10);
  sli.Add(13);
  sli.Add(-5);
  sli.Add(10);
  sli.Add(0);
  sli.Add(10);
  AssertEquals(6, sli.Size, 'Correct size');

  (* Tests: Remove *)
  sli.Remove(10);
  AssertFalse(sli.Contains(10), 'Value removal');
  AssertEquals(3, sli.Size, 'Correct size after removal');

  sli.remove(-5);
  AssertFalse(sli.Contains(-5), 'First value removal');
  AssertEquals(2, sli.Size, 'Correct size after removing first element');

  sli.Done;
END.
