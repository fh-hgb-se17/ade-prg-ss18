UNIT SortedListType;

INTERFACE

  USES ListType;

  TYPE
    SortedList =  OBJECT(List)
      PUBLIC // Methods
        CONSTRUCTOR Init;
        DESTRUCTOR Done;

        (* Inserts the specified value in a sorted manner *)
        PROCEDURE Add(val: INTEGER); VIRTUAL;

        (* See List.Contains *)
        FUNCTION Contains(val: INTEGER): BOOLEAN; VIRTUAL;

        (* See List.Remove *)
        PROCEDURE Remove(val: INTEGER); VIRTUAL;

      PROTECTED // Methods
        (* See List.RemoveNextNodeIfValueIs *)
        PROCEDURE RemoveNextNodeIfValueIs(n: ListNodePtr; val: INTEGER); VIRTUAL;
    END;

IMPLEMENTATION

  (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR SortedList.Init;
  BEGIN
    INHERITED Init;
  END;

  PROCEDURE SortedList.Add(val: INTEGER);
    VAR curr, prev, n: ListNodePtr;
  BEGIN
    Inc(listSize);
    n := CreateNode(val);
    IF first = NIL THEN BEGIN
      first := n;
    END
    ELSE IF n <> NIL THEN BEGIN
      curr := first;
      prev := NIL;
      (* store the first node whose value is *)
      (* >= the specified value in <curr>    *)
      WHILE (curr <> NIL) AND (curr^.value < n^.value) DO BEGIN
        prev := curr;
        curr := curr^.next;
      END;
      n^.next := curr;

      (* if there was no value smaller than the one which *)
      (* was supplied as an argument, n is the new first  *)
      IF prev = NIL THEN
        first := n
      ELSE
        prev^.next := n;
    END;
  END;

  FUNCTION SortedList.Contains(val: INTEGER): BOOLEAN;
    VAR n: ListNodePtr;
  BEGIN
    n := first;
    (* optimization: we can stop as soon as n^.value is equal OR GREATER *)
    (* than the one we are looking for, since the list is sorted         *)
    WHILE (n <> NIL) AND (n^.value < val) DO
      n := n^.next;
    Contains := (n <> NIL) AND (n^.value = val);
  END;

  PROCEDURE SortedList.Remove(val: INTEGER);
    VAR prev, curr: ListNodePtr;
  BEGIN
    (* optimization: we do not have to start searching if *)
    (* the first value is already greater than the one    *)
    (* we want to remove from the list!                   *)
    IF (first <> NIL) AND (first^.value <= val) THEN BEGIN
      RemoveNextNodeIfValueIs(first, val);
      IF (first^.value = val) THEN BEGIN
        prev := first;
        first := first^.next;
        Dispose(prev);
        Dec(listSize);
      END;
    END;
  END;

  DESTRUCTOR SortedList.Done;
  BEGIN
    INHERITED Done;
  END;

  (* ////////// PROTECTED METHODS ////////// *)

  PROCEDURE SortedList.RemoveNextNodeIfValueIs(n: ListNodePtr; val: INTEGER);
    VAR buff: ListNodePtr;
  BEGIN
    IF (n <> NIL) AND (n^.next <> NIL) THEN BEGIN
      (* same optimization as in Sortedlist.Contains:   *)
      (* We do not need to check future nodes if we are *)
      (* already past the sought value                  *)
      IF n^.value <= val THEN
        RemoveNextNodeIfValueIs(n^.next, val);
      IF n^.next^.value = val THEN BEGIN
        buff := n^.next;
        n^.next := n^.next^.next;
        Dispose(buff);
        Dec(listSize);
      END;
    END;
  END;

END.
