UNIT ListType;

INTERFACE

  TYPE
    ListNodePtr = ^ListNode;
    ListNode = RECORD
      next: ListNodePtr;
      value: INTEGER;
    END;

    List = OBJECT
      PUBLIC // Methods
        CONSTRUCTOR Init;
        DESTRUCTOR Done;

        (* Appends value to the back of the List *)
        PROCEDURE Add(val: INTEGER); VIRTUAL;

        (* Returns TRUE when the List contains the *)
        (* specified value at least once           *)
        FUNCTION Contains(val: INTEGER): BOOLEAN; VIRTUAL;

        (* Returns the nr. of elements in the List *)
        FUNCTION Size: INTEGER;

        (* Removes ALL occurrences of the *)
        (* specified value in the List    *)
        PROCEDURE Remove(val: INTEGER); VIRTUAL;

        (* Deletes all elements in the List *)
        PROCEDURE Clear;

      PROTECTED // Members
        (* A pointer to the first ListNode *)
        first: ListNodePtr;
        (* The size of the list *)
        (* Note: for large lists, it's more performant to     *)
        (* increase a counter each time an element is added   *)
        (* rather than iterating through the entire list with *)
        (* every call to List.Size!                           *)
        listSize: INTEGER;

      PROTECTED // Methods
        (* Creates a new node with the specified value *)
        FUNCTION CreateNode(val: INTEGER): ListNodePtr;

        (* Adds a node to the back of the List *)
        PROCEDURE AppendBack(n: ListNodePtr);

        (* Deletes the next ListNode if its value is equal *)
        (* to the one supplied as argument                 *)
        PROCEDURE RemoveNextNodeIfValueIs(n: ListNodePtr; val: INTEGER); VIRTUAL;

        (* Deallocates the memory for the entire list *)
        PROCEDURE DisposeList;
    END;

IMPLEMENTATION

  (* ////////// PUBLIC METHODS ////////// *)

  CONSTRUCTOR List.Init;
  BEGIN
    first := NIL;
    listSize := 0;
  END;

  PROCEDURE List.Add(val: INTEGER);
  BEGIN
    AppendBack(CreateNode(val));
    Inc(listSize);
  END;

  FUNCTION List.Contains(val: INTEGER): BOOLEAN;
    VAR n: ListNodePtr;
  BEGIN
    n := first;
    WHILE (n <> NIL) AND (n^.value <> val) DO
      n := n^.next;
    Contains := n <> NIL;
  END;

  FUNCTION List.Size: INTEGER;
    VAR
      n: ListNodePtr;
      count: INTEGER;
  BEGIN
    Size := listSize;
  END;

  PROCEDURE List.Remove(val: INTEGER);
    VAR prev, curr: ListNodePtr;
  BEGIN
    (* remove all but the first node with matching values *)
    RemoveNextNodeIfValueIs(first, val);

    (* remove first *)
    IF (first <> NIL) AND (first^.value = val) THEN BEGIN
      prev := first;
      first := first^.next;
      Dispose(prev);
      Dec(listSize);
    END;
  END;

  PROCEDURE List.Clear;
  BEGIN
    listSize := 0;
    DisposeList;
  END;

  DESTRUCTOR List.Done;
  BEGIN
    Clear;
  END;

  (* ////////// PROTECTED METHODS ////////// *)

  FUNCTION List.CreateNode(val: INTEGER): ListNodePtr;
    VAR n: ListNodePtr;
  BEGIN
    New(n);
    n^.value := val;
    n^.next := NIL;
    CreateNode := n;
  END;

  PROCEDURE List.AppendBack(n: ListNodePtr);
    VAR buff: ListNodePtr;
  BEGIN
    IF first = NIL THEN
      first := n
    ELSE BEGIN
      buff := first;
      WHILE (buff <> NIL) AND (buff^.next <> NIL) DO
        buff := buff^.next;
      buff^.next := n;
    END;
  END;

  PROCEDURE List.RemoveNextNodeIfValueIs(n: ListNodePtr; val: INTEGER);
    VAR buff: ListNodePtr;
  BEGIN
    IF (n <> NIL) AND (n^.next <> NIL) THEN BEGIN
      (* Remove next node recursively, so the list *)
      (* is deleted "from the back"                *)
      RemoveNextNodeIfValueIs(n^.next, val);

      IF n^.next^.value = val THEN BEGIN
        buff := n^.next;
        n^.next := n^.next^.next;
        Dispose(buff);
        Dec(listSize);
      END;
    END;
  END;

  PROCEDURE List.DisposeList;
  VAR prev: ListNodePtr;
  BEGIN
    WHILE first <> NIL DO BEGIN
      prev := first;
      first := first^.next;
      Dispose(prev);
    END;
  END;

END.
