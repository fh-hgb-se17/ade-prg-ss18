(* Commenting out this preprocessor directive allows for the AssertEquals overloads  *)
(* to be compiled as well. If INT_EQUALS_ONLY is defined, only AssertEquals(int,int) *)
(* and the boolean test procedures will be available.                                *)
{$DEFINE INT_EQUALS_ONLY}
