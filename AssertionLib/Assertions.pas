(* This unit requires that you compile WITHOUT the '-Mtp flag'  *)
(* because it uses a feature called parameter overloading which *)
(* is not supported in turbo pascal!							*)
UNIT Assertions;

INTERFACE

  (* Writes a little test header so subsequent tests *)
  (* are visually separated by any previous output   *)
  PROCEDURE WriteTestSectionName(name: STRING);

  (* Tests whether the actual value of an integer matches the expected one *)
  PROCEDURE AssertEquals(expected, actual: INTEGER; name: STRING);

  (* Tests whether the actual value of an integer matches the expected one *)
  PROCEDURE AssertEquals(expected, actual, name: STRING);

  (* Tests whether the actual value of an integer matches the expected one *)
  PROCEDURE AssertEquals(expected, actual: CHAR; name: STRING);

  (* Tests whether the specified boolean evaluates to TRUE *)
  PROCEDURE AssertTrue(b: BOOLEAN; name: STRING);

  (* Tests whether the specified boolean evaluates to FALSE *)
  PROCEDURE AssertFalse(b: BOOLEAN; name: STRING);

IMPLEMENTATION
  (* The implementation contains a lot of code duplication because I couldn't get  *)
  (* generics to work but since it's a very small unit, code quality is negligible *)

  USES
    Crt,
    sysutils;

  PROCEDURE WriteTestName(name: STRING); FORWARD;
  PROCEDURE WriteIndicatorForCondition(c: BOOLEAN); FORWARD;

  (* ////////// PUBLIC PROCEDURES ////////// *)

  PROCEDURE WriteTestSectionName(name: STRING);
  BEGIN
    WriteLn;
    Write(' ========== ');
    TextColor(LightBlue);
    Write(UpperCase(name), ' TEST');
    TextColor(White);
    WriteLn(' ========== ');
  END;

  PROCEDURE AssertEquals(expected, actual: INTEGER; name: STRING);
    VAR success: BOOLEAN;
  BEGIN
    success := expected = actual;
    WriteTestName(name);
    WriteIndicatorForCondition(success);
    Write('.');
    IF NOT success THEN WriteLn(' Expected ', expected, ', got ', actual)
    ELSE WriteLn;
  END;

  PROCEDURE AssertEquals(expected, actual: CHAR; name: STRING);
    VAR success: BOOLEAN;
  BEGIN
    success := expected = actual;
    WriteTestName(name);
    WriteIndicatorForCondition(success);
    Write('.');
    IF NOT success THEN WriteLn(' Expected ', expected, ', got ', actual)
    ELSE WriteLn;
  END;

  PROCEDURE AssertEquals(expected, actual, name: STRING);
    VAR success: BOOLEAN;
  BEGIN
    success := expected = actual;
    WriteTestName(name);
    WriteIndicatorForCondition(success);
    Write('.');
    IF NOT success THEN WriteLn(' Expected ', expected, ', got ', actual)
    ELSE WriteLn;
  END;

  PROCEDURE AssertTrue(b: BOOLEAN; name: STRING);
  BEGIN
    WriteTestName(name);
    WriteIndicatorForCondition(b);
    Write('.');
    IF NOT b THEN WriteLn(' Expression evaluated to FALSE!')
    ELSE WriteLn;
  END;

  PROCEDURE AssertFalse(b: BOOLEAN; name: STRING);
  BEGIN
    WriteTestName(name);
    WriteIndicatorForCondition(NOT b);
    Write('.');
    IF b THEN WriteLn(' Expression evaluated to TRUE!')
    ELSE WriteLn;
  END;

  (* ////////// PRIVATE PROCEDURES ////////// *)

  PROCEDURE WriteIndicatorForCondition(c: BOOLEAN);
  BEGIN
    IF c THEN BEGIN
      TextColor(Green);
      Write('succeeded');
    END
    ELSE BEGIN
      TextColor(Red);
      Write('failed');
    END;
    TextColor(White);
  END;

  PROCEDURE WriteTestName(name: STRING);
  BEGIN
    Write('Test "', name, '" ');
  END;

END.
