(* ReplacementTable:                              Niklas Vest, 23.04.2018 *)
(* -------                                                                *)
(* Unit for creating a hash table to manage to-be-replaced strings.       *)
(* ====================================================================== *)
UNIT ReplacementTable;

INTERFACE

TYPE
  ListNodePtr = ^ListNode;
  ListNode = RECORD
    original, replacement: String;
    next: ListNodePtr;
  END;

(* Inserts a new original-replacement pair into the hashmap *)
PROCEDURE Insert(original, replacement: String);

(* Returns the replacement for the specified string. *)
(* If the original string has no associated replacement, *)
(* the function returns an empty string. *)
FUNCTION ReplacementFor(original: String): String;

(* Cleanup function *)
PROCEDURE DisposeTable;

IMPLEMENTATION

CONST
  MAX_KEY_VAL = Ord(High(CHAR)) + 1;
  TABLE_SIZE = 31;

VAR
  _words: ARRAY[1..TABLE_SIZE] OF ListNodePtr;

FUNCTION _hash(key: String): INTEGER;
VAR
  h, i: INTEGER;
BEGIN
  h := 0;
  FOR i := 1 TO Length(key) DO BEGIN 
    (*$Q-*)
    (*$R-*)
    h := 31 * h + Ord(key[i]);
    (*$R+*)
    (*$Q+*)
  END;
  _hash := Abs(h) MOD TABLE_SIZE + 1;
END;

FUNCTION _createNode(original, replacement: String): ListNodePtr;
VAR
  n: ListNodePtr;
BEGIN
  New(n);
  n^.original := original;
  n^.replacement := replacement;
  n^.next := NIL;
  _createNode := n;
END;

(* Appends a passed node to the specified list *)
PROCEDURE _appendNode(VAR l: ListNodePtr; n: ListNodePtr);
VAR
  curr: ListNodePtr;
BEGIN
  IF l <> NIL THEN BEGIN
    curr := l;
    WHILE curr^.next <> NIL DO 
      curr := curr^.next;
    curr^.next := n;
  END ELSE l := n;
END;

(* Deletes a list *)
PROCEDURE _disposeList(VAR l: ListNodePtr);
VAR
  curr, pred: ListNodePtr;
BEGIN 
  IF l <> NIL THEN BEGIN
    curr := l;
    WHILE curr <> NIL DO BEGIN 
      pred := curr;
      curr := curr^.next;
      Dispose(pred);
    END;
    l := NIL;
  END;
END;

PROCEDURE Insert(original, replacement: String);
VAR
  ind: INTEGER;
  l, pred: ListNodePtr;
BEGIN
  ind := _hash(original);
  pred := _words[ind];
  
  IF pred = NIL THEN 
  	(* If there hasn't been a key that maps to the same hash as w *)
    _words[ind] := _createNode(original, replacement)
  ELSE BEGIN 
  	(* Else -> check if the key w was already inserted *)
    l := pred^.next;
    WHILE (l <> NIL) AND (pred^.original <> original) DO BEGIN
      pred := l;
      l := l^.next;
    END;
    (* If w exists, increase occurrence counter, else append as new node *)
    IF pred^.original = original THEN
      pred^.replacement := replacement (* Overwrite *)
    ELSE 
      _appendNode(_words[ind], _createNode(original, replacement));
  END;
END;

FUNCTION ReplacementFor(original: String): String;
VAR
  n: ListNodePtr;
BEGIN
  n := _words[_hash(original)];
    WHILE (n <> NIL) AND (n^.original <> original) DO BEGIN 
      n := n^.next;
    END;

    IF n = NIL THEN
      ReplacementFor := ''
    ELSE
      ReplacementFor := n^.replacement;
END;

PROCEDURE DisposeTable;
VAR
  i: INTEGER;
BEGIN
  FOR i := Low(_words) TO TABLE_SIZE DO
    _disposeList(_words[i]);
END;

BEGIN 
  WriteLn('Loading hash table unit (chaining).');
END.