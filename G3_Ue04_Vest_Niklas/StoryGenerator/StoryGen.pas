(* StoryGen:                                      Niklas Vest, 23.04.2018 *)
(* -------                                                                *)
(* A program which transforms stories using a file containing             *)
(* replacements.                                                          *)
(* ====================================================================== *)
PROGRAM StoryGen;

  USES 
    ReplacementTable, Crt;   

  CONST
    Terminators = [' ', '.', ',', '!', '?', CHAR(10)];
    EOFTerminator = '+'; (* just a random char NOT in Terminators *)

  PROCEDURE AssignInput(fileName: String);
  BEGIN 
    Assign(input, fileName);
    Reset(input);
    IF IOResult <> 0 THEN BEGIN 
      WriteLn('Error while opening read stream from file: ', fileName);
      HALT;
    END;
  END;

  PROCEDURE AssignOutput(fileName: String);
  BEGIN 
    Assign(output, fileName);
    Rewrite(output); // TODO rewrite?
    IF IOResult <> 0 THEN BEGIN
      WriteLn('Error while opening write stream to file: ', fileName);
      HALT;
    END;
  END;

  FUNCTION ProcessLine(line: String): BOOLEAN;
    VAR
      original, replacement: String;
      space: INTEGER;
  BEGIN
    ProcessLine := TRUE;
    space := Pos(' ', line);
    IF space > 1 THEN BEGIN 
      original := Copy(line, 1, space - 1);
      replacement := Copy(line, space + 1, Length(line) - Length(original) + 1);
      ReplacementTable.Insert(original, replacement);
    END
    ELSE BEGIN
      ProcessLine := FALSE;
    END;
  END;

  PROCEDURE ProcessReplacementsFile;
    VAR line: String;
  BEGIN
    AssignInput(ParamStr(1));
    REPEAT
      ReadLn(input, line);
    UNTIL NOT ProcessLine(line) OR EOF(input);
    Close(input);
  END;

  PROCEDURE ReadWord(VAR s: String; VAR terminator: CHAR);
    VAR 
      c: CHAR;
  BEGIN
    s := '';
    terminator := EOFTerminator;
    REPEAT
      Read(input, c);
      IF NOT (c IN Terminators) THEN BEGIN
        s := s + String(c);
      END;
    UNTIL EOF(input) OR (c IN Terminators);
    IF c IN Terminators THEN BEGIN
      terminator := c;
    END;
  END;

  PROCEDURE RewriteOldStory;
    VAR 
      w, repl: String;
      terminator: CHAR;
  BEGIN
    AssignInput(ParamStr(2));
    AssignOutput(ParamStr(3));

    REPEAT
      ReadWord(w, terminator);
      repl := ReplacementTable.ReplacementFor(w);

      IF repl = '' THEN
        Write(w)
      ELSE
        Write(repl);
      
      IF terminator IN Terminators THEN
        Write(terminator);
    UNTIL EOF(input);
    Close(input);
    Close(output);
  END;

BEGIN
  IF ParamCount <> 3 THEN BEGIN 
    WriteLn('Wrong number of parameters supplied. Usage:');
    WriteLn('StoryGen replacementsfile oldstory newstory');
  END 
  ELSE BEGIN
    ProcessReplacementsFile;
    RewriteOldStory;
  END;
END.