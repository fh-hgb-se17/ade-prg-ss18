(* CharOccurrenceList:                            Niklas Vest, 23.04.2018 *)
(* -------                                                                *)
(* Unit for storing character values alongside their occurrences within   *)
(* within a text.                                                         *)
(* ====================================================================== *)
UNIT CharOccurrenceList;

INTERFACE

TYPE
  ListNodePtr = ^ListNode;
  ListNode = RECORD
    next: ListNodePtr;
    occurrences: INTEGER;
    value: CHAR;
  END;

(* Either add c to the list or increment the occurrence *)
(* counter if c is the most recently added character. *)
PROCEDURE Push(c: CHAR);

(* Return the full list *)
FUNCTION FetchList: ListNodePtr;

(* Cleanup function *)
PROCEDURE DisposeList;

IMPLEMENTATION

VAR
  list, lastNode: ListNodePtr;

FUNCTION CreateNode(c: CHAR): ListNodePtr;
VAR
  n: ListNodePtr;
BEGIN
  New(n);
  n^.value := c;
  n^.occurrences := 1;
  n^.next := NIL;
  CreateNode := n;
END;

PROCEDURE Push(c: CHAR);
BEGIN
  IF list = NIL THEN BEGIN 
    list := CreateNode(c);
    lastNode := list;
  END
  ELSE IF lastNode^.value = c THEN BEGIN
    lastNode^.occurrences += 1;
  END ELSE BEGIN
    lastNode^.next := CreateNode(c);
    lastNode := lastNode^.next;
  END;
END;

FUNCTION FetchList: ListNodePtr;
BEGIN
  FetchList := list;
END;

PROCEDURE DisposeList;
VAR
  curr, prev: ListNodePtr;
BEGIN 
  curr := list;
  WHILE curr <> NIL DO BEGIN 
    prev := curr;
    curr := curr^.next;
    Dispose(prev);
  END;
END;

BEGIN 
  list := NIL;
  lastNode := NIL;
END.