(* RLE:                                           Niklas Vest, 23.04.2018 *)
(* -------                                                                *)
(* A program that is capable of simple text compression and               *)
(* decomporession.                                                        *)
(* ====================================================================== *)
PROGRAM RLE;

  USES CharOccurrenceList;

  TYPE
    Operation = (Compress, Decompress);

  CONST
    COMPRESSABLES = ['a'..'z', 'A'..'Z', '.', ',', '!', '?'];

  FUNCTION ParseParams(VAR op: Operation; VAR inFile, outFile: String): BOOLEAN;
    VAR 
      valid: BOOLEAN;
      opStr: String;
  BEGIN
    valid := TRUE;
    inFile := '';
    outFile := '';
    op := Compress;

    IF ParamCount > 3 THEN
      valid := FALSE;

    (* at least operation or input file *)
    IF (valid) AND (ParamCount > 0) THEN BEGIN
      IF ParamStr(1)[1] = '-' THEN BEGIN
        opStr := Copy(ParamStr(1), 2, 1);
        IF opStr = 'c' THEN
          op := Compress
        ELSE IF opStr = 'd' THEN
          op := Decompress
        ELSE
          valid := FALSE;
      END ELSE
        inFile := ParamStr(1);
    END;

    (* either operation and input file or input file and output file *)
    IF (valid) AND (ParamCount > 1) THEN BEGIN
      IF ParamStr(2)[1] = '-' THEN
        valid := FALSE
      ELSE IF inFile = '' THEN
        inFile := ParamStr(2)
      ELSE
        outFile := ParamStr(2);
    END;

    (* operation, input file and output file *)
    IF (valid) AND (ParamCount = 3) THEN BEGIN
      IF ParamStr(3)[1] = '-' THEN
          valid := FALSE
      ELSE
        outFile := ParamStr(3);
    END;

    ParseParams := valid;
  END;

  PROCEDURE BindInput(inFile: String);
  BEGIN 
    IF inFile <> '=' THEN BEGIN 
      Assign(input, inFile);
      Reset(input);
      IF IOResult <> 0 THEN BEGIN 
        WriteLn('Error while reading input file "', inFile, '".');
        HALT;
      END;
    END;
  END;

  PROCEDURE BindOutput(outFile: String);
  BEGIN 
    IF outFile <> '=' THEN BEGIN 
      Assign(output, outFile);
      Rewrite(output);
      IF IOResult <> 0 THEN BEGIN 
        WriteLn('Error while writing ouput file "', outFile, '".');
        HALT;
      END;
    END;
  END;

  PROCEDURE CompressInput(inFile, outFile: String);
    VAR
      c: CHAR;
      list: ListNodePtr;
  BEGIN
    BindInput(inFile);
    REPEAT 
      Read(input, c);
      CharOccurrenceList.Push(c);
    UNTIL EOF(input);

    BindOutput(outFile);
    list := CharOccurrenceList.FetchList;
    WHILE list <> NIL DO BEGIN
      CASE list^.occurrences OF
        1: Write(list^.value);
        2: Write(list^.value, list^.value);
        ELSE Write(list^.value, list^.occurrences);
      END;
      list := list^.next;
    END;
    CharOccurrenceList.DisposeList;
  END;

  FUNCTION InDigitRange(c: CHAR): BOOLEAN;
  BEGIN 
    InDigitRange := (Ord(c) >= 51) (*3*) AND (Ord(c) <= 57); (*9*)
  END;

  PROCEDURE DecompressInput(inFile, outFile: String);
    VAR
      currentChar, newChar: CHAR;
      i, currentIterations: INTEGER;
  BEGIN
    BindInput(inFile);
    BindOutput(outFile);
    currentChar := ' ';
    REPEAT
      Read(input, newChar);
      IF newChar IN COMPRESSABLES THEN BEGIN
        currentChar := newChar;
        Write(currentChar);
      END ELSE IF InDigitRange(newChar) AND (currentChar <> ' ') THEN BEGIN
        currentIterations := 9 - (Ord('9') - Ord(newChar));
        FOR i := 2 TO currentIterations DO BEGIN 
          Write(output, currentChar);
        END;
      END;
    UNTIL EOF(input);
  END;

  VAR
    op: Operation;
    inFile, outFile: String;

BEGIN
  IF ParseParams(op, inFile, outFile) THEN BEGIN
    CASE op OF
      Compress: CompressInput(inFile, outFile);
      Decompress: DecompressInput(inFile, outFile);
    END;
  END ELSE BEGIN 
    WriteLn('Invalid parameters. Usage:');
    WriteLn('RLE [ -c | -d ] [ input [ output ] ]');
  END;
END.