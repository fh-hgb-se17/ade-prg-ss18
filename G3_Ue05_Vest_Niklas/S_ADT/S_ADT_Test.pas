PROGRAM S_ADT_Test;

  USES
    S_ADT;

  VAR
    s1, s2: Stack;
    i, val: INTEGER;

BEGIN

  CreateStack(s1);
  CreateStack(s2);

  FOR i := 1 TO 10 DO BEGIN 
    Push(s1, i);
    Push(s2, i*i);
  END;

  Push(s2, -1);

  Write('Emptying stack 1: ');
  WHILE NOT IsEmpty(s1) DO BEGIN 
    Pop(s1, val);
    Write(val, ' ');
  END;
  WriteLn;
  
  Write('Emptying stack 2: ');
  WHILE NOT IsEmpty(s2) DO BEGIN 
    POP(s2, val);
    Write(val, ' ');    
  END;
END.