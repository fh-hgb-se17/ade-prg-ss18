UNIT S_ADT;

INTERFACE

  TYPE
    Stack = POINTER;

  PROCEDURE CreateStack(VAR s: Stack);
  PROCEDURE Push(VAR s: Stack; val: INTEGER);
  PROCEDURE Pop(VAR s: Stack; VAR val: INTEGER);
  FUNCTION IsEmpty(VAR s: Stack): BOOLEAN;
  PROCEDURE DisposeStack(VAR s: Stack);

IMPLEMENTATION

  USES
    V_ADT;

  PROCEDURE CreateStack(VAR s: Stack);
  BEGIN 
    CreateVector(Vector(s));
  END;

  PROCEDURE Push(VAR s: Stack; val: INTEGER);
  BEGIN 
    Add(Vector(s), val);
  END;

  PROCEDURE Pop(VAR s: Stack; VAR val: INTEGER);
  BEGIN 
    IF NOT IsEmpty(s) THEN BEGIN 
      GetElementAt(Vector(s), Size(Vector(s)), val);
      RemoveElementAt(Vector(s), Size(Vector(s)));
    END;
  END;

  FUNCTION IsEmpty(VAR s: Stack): BOOLEAN;
  BEGIN 
    IsEmpty := Size(Vector(s)) = 0;
  END;

  PROCEDURE DisposeStack(VAR s: Stack);
  BEGIN 
    DisposeVector(Vector(s));
  END;

END.