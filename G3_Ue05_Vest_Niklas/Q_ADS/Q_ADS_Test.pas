PROGRAM Q_ADS_Test;

  USES
    Q_ADS;

  VAR
    i, val: INTEGER;

BEGIN
  FOR i := 1 TO 10 DO BEGIN 
    Enqueue(i * i);
  END;

  WriteLn('Emptying queue: ');
  WHILE NOT IsEmpty DO BEGIN 
    Dequeue(val);
    Write(val, ' ');
  END;
END.