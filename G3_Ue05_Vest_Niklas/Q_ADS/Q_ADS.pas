UNIT Q_ADS;

INTERFACE

  PROCEDURE Enqueue(val: INTEGER);
  PROCEDURE Dequeue(VAR val: INTEGER);
  FUNCTION IsEmpty: BOOLEAN;

  PROCEDURE Debug;

IMPLEMENTATION

  USES
    V_ADS;

  PROCEDURE Debug;
    VAR i, val: INTEGER;
  BEGIN 
    FOR i := 1 TO Size DO BEGIN
      GetElementAt(i, val);
      WriteLn(i, ': ', val);
    END;
  END;

  PROCEDURE Enqueue(val: INTEGER);
  BEGIN 
    Add(val);
  END;

  PROCEDURE Dequeue(VAR val: INTEGER);
  BEGIN 
    IF Size > 0 THEN BEGIN
      GetElementAt(1, val);
      RemoveElementAt(1);
    END
    ELSE BEGIN
      val := 0;
    END;
  END;

  FUNCTION IsEmpty: BOOLEAN;
  BEGIN 
    IsEmpty := V_ADS.Size = 0;
  END;

END.