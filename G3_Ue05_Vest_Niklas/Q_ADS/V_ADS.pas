UNIT V_ADS;

INTERFACE

  (* Adds val to the vector *)
  PROCEDURE Add(val: INTEGER);

  (* If pos is not out of range for the vector, the vector at position *)
  (* pos is set to val, otherwise val is simply "Add"ed.               *)
  PROCEDURE SetElementAt(pos: INTEGER; val: INTEGER);

  (* If pos is not out of range for the vector, val is set to the the *)
  (* value of the element at position pos within the vector.          *)
  PROCEDURE GetElementAt(pos: INTEGER; VAR val: INTEGER);

  (* If pos is not out of range for the vector, the element at index *)
  (* pos is removed and all latter elements are shifted left.        *)
  PROCEDURE RemoveElementAt(pos: INTEGER);

  (* Returns the number of items in the vector. *)
  FUNCTION Size: INTEGER;

  (* Returns the number of current capacity of the vector *)
  FUNCTION Capacity: INTEGER;

  (* Completely obliterates dat boi *)
  PROCEDURE DisposeVector;

IMPLEMENTATION

  CONST
    (* Default size, makes it easy to test reallocation *)
    INIT_VECTOR_SIZE = 5;

  TYPE
    IntArray = ARRAY[1..1] OF INTEGER;
    DynamicIntArray = ^IntArray;
    VectorRec = RECORD
      capacity: INTEGER;
      size: INTEGER;
      elements: DynamicIntArray;
    END;

  VAR
    vector: VectorRec;

  PROCEDURE IndexOutOfBounds(got, max: INTEGER);
  BEGIN 
    WriteLn('ERROR: index ', got, ' is out of range for vector of size ', max);
  END;

  (* Reallocates the whole array with double the size *)
  PROCEDURE Resize;
    VAR
      newArr: DynamicIntArray;
      i: INTEGER;
  BEGIN
    GetMem(newArr, vector.capacity * 2 * SizeOf(INTEGER));
    FOR i := 1 TO vector.size DO BEGIN 
      (*$R-*) newArr^[i] := vector.elements^[i]; (*$R+*)
    END;

    FreeMem(vector.elements, vector.capacity * SizeOf(INTEGER));

    vector.elements := newArr;
    vector.capacity := vector.capacity * 2;
  END;

  PROCEDURE Add(val: INTEGER);
  BEGIN
    IF vector.size = vector.capacity THEN BEGIN
      Resize;
    END;

    Inc(vector.size);
    (*$R-*) vector.elements^[vector.size] := val; (*$R+*)
  END;

  PROCEDURE SetElementAt(pos: INTEGER; val: INTEGER);
  BEGIN
    IF (pos > 0) AND (pos <= vector.size) THEN BEGIN
      (*$R-*) vector.elements^[pos] := val; (*$R+*)
    END 
    ELSE IF pos > 0 THEN BEGIN 
      Add(val);
    END
    ELSE BEGIN 
      IndexOutOfBounds(pos, vector.size);
    END;
  END;

  PROCEDURE GetElementAt(pos: INTEGER; VAR val: INTEGER);
  BEGIN
    val := 0;
    IF (pos > 0) AND (pos <= vector.size) THEN BEGIN 
      (*$R-*) val := vector.elements^[pos]; (*$R+*)
    END 
    ELSE BEGIN
      IndexOutOfBounds(pos, vector.size);
    END;
  END;

  PROCEDURE RemoveElementAt(pos: INTEGER);
    VAR
      i: INTEGER;
  BEGIN
    IF (pos > 0) AND (pos <= vector.size) THEN BEGIN
      (* shift elements left *)
      FOR i := pos TO (vector.size - 1) DO BEGIN 
        (*$R-*) vector.elements^[i] := vector.elements^[i + 1]; (*$R+*)
      END;
      Dec(vector.size);
    END
    ELSE BEGIN 
      IndexOutOfBounds(pos, vector.size);
    END;
  END;

  FUNCTION Size: INTEGER;
  BEGIN 
    Size := vector.size;
  END;

  FUNCTION Capacity: INTEGER;
  BEGIN 
    Capacity := vector.capacity;
  END;

  PROCEDURE DisposeVector;
  BEGIN 
    FreeMem(vector.elements, vector.capacity * SizeOf(INTEGER));
  END;

BEGIN
  vector.capacity := INIT_VECTOR_SIZE;
  vector.size := 0;
  GetMem(vector.elements, vector.capacity * SizeOf(INTEGER));
END.