PROGRAM V_ADS_Test;

  USES
    V_ADS;

  PROCEDURE PrintVec;
    VAR
      i, val: INTEGER;
  BEGIN
    (* Size *)
    Write('Vector of size ', Size, ': ');
    FOR i := 1 TO Size DO BEGIN
      GetElementAt(i, val); 
      Write(val, ' ');
    END;
    WriteLn;
  END;

  VAR
    i, val: INTEGER;

BEGIN
  (* Capacity *)
  WriteLn('Current capacity is ', Capacity);

  (* Add *)
  FOR i := 1 TO 10 DO BEGIN 
    Add(i*2); (* forces resize from 5 to 10 *)
  END;
  PrintVec;

  (* SetElementAt and private Resize *)
  SetElementAt(3, 99);
  SetElementAt(5, 1);
  SetElementAt(1001, -1); (* forces resize from 10 to 20 *)
  PrintVec;

  WriteLn('Current capacity is ', Capacity);

  (* RemoveElementAt *)
  RemoveElementAt(1);
  RemoveElementAt(5);
  RemoveElementAt(1002);
  PrintVec;

  (* RangeCheck on GetElementAt *)
  GetElementAt(1003, val);

  DisposeVector;
END.