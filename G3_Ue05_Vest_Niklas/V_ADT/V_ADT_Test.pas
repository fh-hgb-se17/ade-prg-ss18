PROGRAM V_ADT_Test;

  USES
    V_ADT;

  PROCEDURE PrintVec(VAR v: Vector);
    VAR
      i, val: INTEGER;
  BEGIN 
    Write('Vector of size ', Size(v), ': ');
    FOR i := 1 TO Size(v) DO BEGIN
      GetElementAt(v, i, val); 
      Write(val, ' ');
    END;
    WriteLn;
  END;

  VAR
    v1, v2: Vector;
    i: INTEGER;

BEGIN 
  (* Only a small amount of tests since the relevant *)
  (* tests are located in V_ADS_Test.pas             *)

  CreateVector(v1);
  CreateVector(v2);

  FOR i := 1 TO 10 DO BEGIN 
    Add(v1, i);
    Add(v2, i * 5);
  END;

  PrintVec(v1);
  PrintVec(v2);

  SetElementAt(v1, 4, 666); (* hehe *)
  SetElementAt(v2, 1001, 999);

  PrintVec(v1);
  PrintVec(v2);

  WriteLn('Capacity of v2 is ', Capacity(v2));
END.