UNIT V_ADT;

INTERFACE

  TYPE
    Vector = POINTER;

  (* Assigns a pointer to an initialized vector to v. *)
  PROCEDURE CreateVector(VAR v: Vector);

  (* Adds val to the vector *)
  PROCEDURE Add(VAR v: Vector; val: INTEGER);

  (* If pos is not out of range for the vector, the vector at position *)
  (* pos is set to val, otherwise val is simply "Add"ed.               *)
  PROCEDURE SetElementAt(VAR v: Vector; pos: INTEGER; val: INTEGER);

  (* If pos is not out of range for the vector, val is set to the the *)
  (* value of the element at position pos within the Vec(v)^.          *)
  PROCEDURE GetElementAt(VAR v: Vector; pos: INTEGER; VAR val: INTEGER);

  (* If pos is not out of range for the vector, the element at index *)
  (* pos is removed and all latter elements are shifted left.        *)
  PROCEDURE RemoveElementAt(VAR v: Vector; pos: INTEGER);

  (* Returns the number of items in the Vec(v)^. *)
  FUNCTION Size(VAR v: Vector): INTEGER;

  (* Returns the number of current capacity of the vector *)
  FUNCTION Capacity(VAR v: Vector): INTEGER;

  (* Frees memory associated with v *)
  PROCEDURE DisposeVector(VAR v: Vector);

IMPLEMENTATION

  CONST
    (* Default size, makes it easy to test reallocation *)
    INIT_VECTOR_SIZE = 5;

  TYPE
    IntArray = ARRAY[1..1] OF INTEGER;
    DynamicIntArray = ^IntArray;
    VectorRec = RECORD
      capacity: INTEGER;
      size: INTEGER;
      elements: DynamicIntArray;
    END;
    Vec = ^VectorRec;

  PROCEDURE IndexOutOfBounds(got, max: INTEGER);
  BEGIN 
    WriteLn('ERROR: index ', got, ' is out of range for vector of size ', max);
  END;

  (* Reallocates the whole array with double the size *)
  PROCEDURE Resize(VAR v: Vector);
    VAR
      newArr: DynamicIntArray;
      i: INTEGER;
  BEGIN
    GetMem(newArr, Vec(v)^.capacity * 2 * SizeOf(INTEGER));
    FOR i := 1 TO Vec(v)^.size DO BEGIN 
      (*$R-*) newArr^[i] := Vec(v)^.elements^[i]; (*$R+*)
    END;

    FreeMem(Vec(v)^.elements, Vec(v)^.capacity * SizeOf(INTEGER));

    Vec(v)^.elements := newArr;
    Vec(v)^.capacity := Vec(v)^.capacity * 2;
  END;

  PROCEDURE CreateVector(VAR v: Vector);
    VAR
      newVec: Vec;
  BEGIN
    New(newVec);
    newVec^.capacity := INIT_VECTOR_SIZE;
    newVec^.size := 0;
    GetMem(newVec^.elements, newVec^.capacity * SizeOf(INTEGER));
    v := POINTER(newVec);
  END;

  PROCEDURE Add(VAR v: Vector; val: INTEGER);
  BEGIN
    IF Vec(v)^.size = Vec(v)^.capacity THEN BEGIN
      Resize(v);
    END;

    Inc(Vec(v)^.size);
    (*$R-*) Vec(v)^.elements^[Vec(v)^.size] := val; (*$R+*)
  END;

  PROCEDURE SetElementAt(VAR v: Vector; pos: INTEGER; val: INTEGER);
  BEGIN
    IF (pos > 0) AND (pos <= Vec(v)^.size) THEN BEGIN
      (*$R-*) Vec(v)^.elements^[pos] := val; (*$R+*)
    END 
    ELSE IF pos > 0 THEN BEGIN 
      Add(v, val);
    END
    ELSE BEGIN 
      IndexOutOfBounds(pos, Vec(v)^.size);      
    END;
  END;

  PROCEDURE GetElementAt(VAR v: Vector; pos: INTEGER; VAR val: INTEGER);
  BEGIN
    val := 0;
    IF (pos > 0) AND (pos <= Vec(v)^.size) THEN BEGIN 
      (*$R-*) val := Vec(v)^.elements^[pos]; (*$R+*)
    END 
    ELSE BEGIN 
      IndexOutOfBounds(pos, Vec(v)^.size);
    END;
  END;

  PROCEDURE RemoveElementAt(VAR v: Vector; pos: INTEGER);
    VAR
      i: INTEGER;
  BEGIN
    IF (pos > 0) AND (pos <= Vec(v)^.size) THEN BEGIN
      (* shift elements left *)
      FOR i := pos TO (Vec(v)^.size - 1) DO BEGIN 
        (*$R-*) Vec(v)^.elements^[i] := Vec(v)^.elements^[i + 1]; (*$R+*)
      END;
      Dec(Vec(v)^.size);
    END
    ELSE BEGIN 
      IndexOutOfBounds(pos, Vec(v)^.size);
    END;
  END;

  FUNCTION Size(VAR v: Vector): INTEGER;
  BEGIN 
    Size := Vec(v)^.size;
  END;

  FUNCTION Capacity(VAR v: Vector): INTEGER;
  BEGIN 
    Capacity := Vec(v)^.capacity;
  END;

  PROCEDURE DisposeVector(VAR v: Vector);
  BEGIN 
    FreeMem(Vec(v)^.elements, Vec(v)^.capacity * SizeOf(INTEGER));
  END;

END.