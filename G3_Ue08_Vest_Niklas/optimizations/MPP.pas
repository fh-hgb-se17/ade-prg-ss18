PROGRAM MPP;

  USES
    //WinCrt,
    MP_Lex,
    MPP_SS,
    CodeGen,
    CodeInt,
    CodeDef;

  VAR
    srcName: STRING;
    ok: BOOLEAN;
    ca: CodeArray;

BEGIN (*MP_P*)

  WriteLn('MPP: MiniPascal Parser');

  WHILE TRUE DO BEGIN
    WriteLn;
    Write('MiniPascal source file (*.mp) > ');
    ReadLn(srcName);
    IF Length(srcName) = 0 THEN
      Exit;

    InitScanner(srcName, ok);
    IF ok THEN BEGIN
      S; (*parsing*)
      GetCode(ca);
      InterpretCode(ca);
    END ELSE
      WriteLn('... File not found');

  END; (*WHILE*)

END. (*MPP*)
