UNIT CodeGen;

INTERFACE

  USES
    CodeDef,
    StringTree,
    SymTab;

  PROCEDURE InitCodeGenerator;

  PROCEDURE Emit1(opc: OpCode);
  PROCEDURE Emit2(opc: OpCode; opd: INTEGER);

  PROCEDURE EmitCodeForExprTree(syntaxTree: TreePtr);

  FUNCTION  CurAddr: INTEGER;
  PROCEDURE FixUp(addr: INTEGER; opd: INTEGER);

  PROCEDURE GetCode(VAR ca: CodeArray);


IMPLEMENTATION

  VAR
    ca: CodeArray; (*array of opCodes and opderands*)
    n: INTEGER;    (*index of next free byte in c*)


  PROCEDURE InitCodeGenerator;
(*-----------------------------------------------------------------*)
    VAR
      i: INTEGER;
  BEGIN
    n := 1;
    FOR i := 1 TO maxCodeLen DO BEGIN
      ca[i] := 0;
    END; (*FOR*)
  END; (*InitCodeGenerator*)


  PROCEDURE EmitByte(b: BYTE);
  BEGIN
    IF n = maxCodeLen THEN BEGIN
      WriteLn('*** Error: overflow in code array');
      HALT;
    END; (*IF*)
    ca[n] := b;
    n := n + 1;
  END; (*EmitByte*)

  PROCEDURE EmitWord(w: INTEGER);
  BEGIN
    EmitByte(w DIV 256);
    EmitByte(w MOD 256);
  END; (*EmitWord*)


  PROCEDURE Emit1(opc: OpCode);
(*-----------------------------------------------------------------*)
  BEGIN
    EmitByte(Ord(opc));
  END; (*Emit1*)

  PROCEDURE Emit2(opc: OpCode; opd: INTEGER);
(*-----------------------------------------------------------------*)
  BEGIN
    EmitByte(Ord(opc));
    EmitWord(opd);
  END; (*Emit2*)


  FUNCTION CurAddr: INTEGER;
(*-----------------------------------------------------------------*)
  BEGIN
    CurAddr := n;
  END; (*CurAddr*)

  PROCEDURE FixUp(addr: INTEGER; opd: INTEGER);
(*-----------------------------------------------------------------*)
  BEGIN
    ca[addr    ] := opd DIV 256;
    ca[addr + 1] := opd MOD 256;
  END; (*FixUp*)


  PROCEDURE GetCode(VAR ca: CodeArray);
(*-----------------------------------------------------------------*)
  BEGIN
    ca := CodeGen.ca;
  END; (*GetCode*)

  PROCEDURE EmitOperationCode(operation: CHAR);
(*-----------------------------------------------------------------*)
  BEGIN
    CASE operation OF
      '+': Emit1(AddOpc);
      '-': Emit1(SubOpc);
      '*': Emit1(MulOpc);
      '/': Emit1(DivOpc);
    END;
  END;

  PROCEDURE EmitOptimizedCode(syntaxTree: TreePtr);
(*-----------------------------------------------------------------*)
  VAR
      numberVal: INTEGER;
      ok: INTEGER;
  BEGIN
    IF syntaxTree <> NIL THEN BEGIN
      EmitOptimizedCode(syntaxTree^.left);
      EmitOptimizedCode(syntaxTree^.right);
      CASE syntaxTree^.valType OF
        Operation: BEGIN
          EmitOperationCode(syntaxTree^.value[1]);
        END;
        Identifier: BEGIN
          Emit2(LoadValOpc, AddrOf(syntaxTree^.value));
        END;
        Literal: BEGIN
          Val(syntaxTree^.value, numberVal, ok);
          Emit2(LoadConstOpc, numberVal);
        END;
      END;
    END;
  END;

  PROCEDURE EmitCodeForExprTree(syntaxTree: TreePtr);
(*-----------------------------------------------------------------*)
  BEGIN
    IF syntaxTree <> NIL THEN BEGIN
      OptimizeArithmeticExpr(syntaxTree);
      PrintTree(syntaxTree);
      WriteLn;
      EmitOptimizedCode(syntaxTree);
    END;
  END;
END. (*CodeGen*)
