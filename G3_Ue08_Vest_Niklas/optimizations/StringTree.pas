UNIT StringTree;

INTERFACE

  TYPE
    ValueType = (Operation, Literal, Identifier);
    NodePtr = ^Node;
    Node = RECORD
      value: STRING;
      valType: ValueType;
      left, right: NodePtr;
    END;
    TreePtr = NodePtr;

  (* Creates a new tree node with the specified value and value type *)
  FUNCTION CreateNode(value: STRING; v: ValueType): NodePtr;

  (* Appends the supplied node to the left of the passed tree *)
  PROCEDURE AppendLeft(tree: TreePtr; node: NodePtr);

  (* Appends the supplied node to the right of the passed tree *)
  PROCEDURE AppendRight(tree: TreePtr; node: NodePtr);

  (* Performs basic optimization for an arithmetic expression *)
  PROCEDURE OptimizeArithmeticExpr(VAR syntaxTree: TreePtr);

  (* Prints a tree in-order *)
  PROCEDURE PrintTree(tree: TreePtr);

  (* Cleans data associated with the specified tree *)
  PROCEDURE MoveTreeToRecycleBin(VAR tree: TreePtr);

IMPLEMENTATION

  FUNCTION CreateNode(value: STRING; v: ValueType): NodePtr;
    VAR n: NodePtr;
  BEGIN
    New(n);
    n^.value := value;
    n^.valType := v;
    n^.left := NIL;
    n^.right := NIL;
    CreateNode := n;
  END;

  (* Moves all data from the source node to the destination port *)
  (* and deletes the source node afterwards                      *)
  PROCEDURE MoveNodeData(source, destination: NodePtr);
  BEGIN
    IF (source <> NIL) AND (destination <> NIL) THEN BEGIN
      destination^.left := source^.left;
      destination^.right := source^.right;
      destination^.valType := source^.valType;
      destination^.value := source^.value;

      (* "unpoint" the children so the tree is not *)
      (* deleted recursively *)
      source^.left := NIL;
      source^.right := NIL;
      MoveTreeToRecycleBin(source);
    END;
  END;

  PROCEDURE AppendLeft(tree: TreePtr; node: NodePtr);
  BEGIN
    IF (tree <> NIL) AND (tree^.left = NIL) THEN BEGIN
      tree^.left := node;
    END;
  END;

  PROCEDURE AppendRight(tree: TreePtr; node: NodePtr);
  BEGIN
    IF (tree <> NIL) AND (tree^.right = NIL) THEN BEGIN
      tree^.right := node;
    END;
  END;

  (* Returns true if the syntaxTree represents an expression *)
  (* which only involves constants                           *)
  (* LITERAL ('+'|'-'|'*'|'/') LITERAL                       *)
  FUNCTION IsConstantOperationTree(syntaxTree: TreePtr): BOOLEAN;
  BEGIN
    IsConstantOperationTree := (syntaxTree <> NIL)
          AND (syntaxTree^.valType = Operation)
          AND (syntaxTree^.left <> NIL)
          AND (syntaxTree^.right <> NIL)
          AND (syntaxTree^.left^.valType = Literal)
          AND (syntaxTree^.right^.valType = Literal);
  END;

  (* evaluates a constant expression so this calculation *)
  (* does not impact runtime performance                 *)
  PROCEDURE OptimizeBothConstants(VAR syntaxTree: TreePtr);
    VAR opd1, opd2, ok: INTEGER;
  BEGIN
    IF IsConstantOperationTree(syntaxTree) THEN BEGIN
      Val(syntaxTree^.left^.value, opd1, ok);
      Val(syntaxTree^.right^.value, opd2, ok);
      CASE syntaxTree^.value[1] OF
        '+': Str(opd1 + opd2, syntaxTree^.value);
        '-': Str(opd1 - opd2, syntaxTree^.value);
        '*': Str(opd1 * opd2, syntaxTree^.value);
        '/': Str(opd1 DIV opd2, syntaxTree^.value);
      END;
      syntaxTree^.valType := Literal;
      MoveTreeToRecycleBin(syntaxTree^.left);
      MoveTreeToRecycleBin(syntaxTree^.right);
    END;
  END;

  (* Returns true if the supplied node represents the specified constant *)
  FUNCTION IsConstant(syntaxTree: NodePtr; ch: CHAR): BOOLEAN;
  BEGIN
    IsConstant := (syntaxTree <> NIL) AND (syntaxTree^.value = ch);
  END;

  (* Small helper function for checking if the operator is *)
  (* ('+'|'-') or ('*'|'/')                                *)
  FUNCTION IsEitherOperator(val, option1, option2: CHAR): BOOLEAN;
  BEGIN
    IsEitherOperator := (val = option1) OR (val = option2);
  END;

  (* Tries to optimize addition and subtraction with *)
  (* 0 for the supplied tree                         *)
  PROCEDURE OptimizeAddSub0(VAR syntaxTree: TreePtr);
  BEGIN
    IF (syntaxTree <> NIL) AND IsEitherOperator(syntaxTree^.value[1], '+', '-') THEN BEGIN
      IF IsConstant(syntaxTree^.left, '0') THEN BEGIN
        MoveTreeToRecycleBin(syntaxTree^.left);
        MoveNodeData(syntaxTree^.right, syntaxTree);
      END
      ELSE IF IsConstant(syntaxTree^.right, '0') THEN BEGIN
        MoveTreeToRecycleBin(syntaxTree^.right);
        MoveNodeData(syntaxTree^.left, syntaxTree);
      END;
    END;
  END;

  (* Tries to optimize multiplication and division with *)
  (* 1 for the supplied tree                            *)
  PROCEDURE OptimizeMulDiv1(VAR syntaxTree: TreePtr);
  BEGIN
    IF (syntaxTree <> NIL) AND IsEitherOperator(syntaxTree^.value[1], '*', '/') THEN BEGIN
      IF IsConstant(syntaxTree^.left, '1') THEN BEGIN
        MoveTreeToRecycleBin(syntaxTree^.left);
        MoveNodeData(syntaxTree^.right, syntaxTree);
      END
      ELSE IF IsConstant(syntaxTree^.right, '1') THEN BEGIN
        MoveTreeToRecycleBin(syntaxTree^.right);
        MoveNodeData(syntaxTree^.left, syntaxTree);
      END;
    END;
  END;

  (* Optimizes operations so that the conjunction with the *)
  (* operation's respective neutral element are removed and*)
  (* constant expressions are evaluated at compile time    *)
  PROCEDURE OptimizeUselessOperations(VAR syntaxTree: TreePtr);
  BEGIN
    IF syntaxTree <> NIL THEN BEGIN
      OptimizeAddSub0(syntaxTree);
      OptimizeMulDiv1(syntaxTree);
    END;
  END;

  (* Recursively optimizes the arithmetical operation *)
  (* represented by this syntax tree                  *)
  PROCEDURE OptimizeArithmeticExpr(VAR syntaxTree: TreePtr);
  BEGIN
    IF syntaxTree <> NIL THEN BEGIN
      OptimizeArithmeticExpr(syntaxTree^.left);
      OptimizeArithmeticExpr(syntaxTree^.right);
      OptimizeBothConstants(syntaxTree);
      OptimizeUselessOperations(syntaxTree);
    END;
  END;

  PROCEDURE PrintTree(tree: TreePtr);
  BEGIN
    IF tree <> NIL THEN BEGIN
      IF tree^.left <> NIL THEN Write('(');
      PrintTree(tree^.left);
      Write(tree^.value);
      PrintTree(tree^.right);
      IF tree^.left <> NIL THEN Write(')');
    END;
  END;

  PROCEDURE MoveTreeToRecycleBin(VAR tree: TreePtr);
  BEGIN
    IF tree <> NIL THEN BEGIN
      MoveTreeToRecycleBin(tree^.left);
      MoveTreeToRecycleBin(tree^.right);
      Dispose(tree);
      tree := NIL;
    END;
  END;

END.
