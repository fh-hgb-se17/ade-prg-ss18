UNIT MPP_SS;

INTERFACE

  VAR
    success: BOOLEAN; (*true if no syntax errros*)

  PROCEDURE S;        (*parses whole MiniPascal program*)


IMPLEMENTATION

  USES
    MP_Lex,
    SymTab,
    CodeGen,
    CodeDef,
    StringTree;

  FUNCTION SyIsNot(expectedSy: Symbol): BOOLEAN;
  BEGIN
    success:= success AND (sy = expectedSy);
    SyIsNot := NOT success;
  END; (*SyIsNot*)

  PROCEDURE SemErr(s: String);
  BEGIN
    WriteLn(s);
    Halt;
  END;


  PROCEDURE MP;      FORWARD;
  PROCEDURE VarDecl; FORWARD;
  PROCEDURE StatSeq; FORWARD;
  PROCEDURE Stat;    FORWARD;
  FUNCTION Expr: TreePtr;    FORWARD;
  PROCEDURE Term(VAR syntaxTree: TreePtr);    FORWARD;
  PROCEDURE Fact(VAR syntaxTree: TreePtr);    FORWARD;

  PROCEDURE S;
(*-----------------------------------------------------------------*)
  BEGIN
    WriteLn('parsing started ...');
    success := TRUE;
    MP;
    IF NOT success OR SyIsNot(eofSy) THEN
      WriteLn('*** Error in line ', syLnr:0, ', column ', syCnr:0)
    ELSE
      WriteLn('... parsing ended successfully ');
  END; (*S*)

  PROCEDURE MP;
  BEGIN
    (*SEM*) InitSymbolTable; (*ENDSEM*)
    (*SEM*) InitCodeGenerator; (*ENDSEM*)
    IF SyIsNot(programSy) THEN Exit;
    NewSy;
    IF SyIsNot(identSy) THEN Exit;
    NewSy;
    IF SyIsNot(semicolonSy) THEN Exit;
    NewSy;
    IF sy = varSy THEN BEGIN
      VarDecl; IF NOT success THEN Exit;
    END; (*IF*)
    IF SyIsNot(beginSy) THEN Exit;
    NewSy;
    StatSeq; IF NOT success THEN Exit;
    (*SEM*) Emit1(EndOpc); (*ENDSEM*)
    IF SyIsNot(endSy) THEN Exit;
    NewSy;
    IF SyIsNot(periodSy) THEN Exit;
    NewSy;
  END; (*MP*)

  PROCEDURE VarDecl;
  VAR ok: BOOLEAN;
  BEGIN
    IF SyIsNot(varSy) THEN Exit;
    NewSy;
    IF SyIsNot(identSy) THEN Exit;
    DeclVar(identStr, ok);
    NewSy;
    WHILE sy = commaSy DO BEGIN
      NewSy;
      IF SyIsNot(identSy) THEN Exit;
      (*SEM*) DeclVar(identStr, ok); (*ENDSEM*)
      (*SEM*) IF NOT ok THEN
      SemErr('Multiple Declarations'); (*ENDSEM*)
      NewSy;
    END; (*WHILE*)
    IF SyIsNot(colonSy) THEN Exit;
    NewSy;
    IF SyIsNot(integerSy) THEN Exit;
    NewSy;
    IF SyIsNot(semicolonSy) THEN Exit;
    NewSy;
  END; (*VarDecl*)

  PROCEDURE StatSeq;
  BEGIN
    Stat; IF NOT success THEN Exit;
    WHILE sy = semicolonSy DO BEGIN
      NewSy;
      Stat; IF NOT success THEN Exit;
    END; (*WHILE*)
  END; (*StatSeq*)

  PROCEDURE Stat;
  VAR
    destId: String;
    addr1, addr2: INTEGER;
  BEGIN
    CASE sy OF
      identSy: BEGIN
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE
            Emit2(LoadAddrOpc, AddrOf(identStr)); (*ENDSEM*)
          NewSy;
          IF SyIsNot(assignSy) THEN Exit;
          NewSy;
          EmitCodeForExprTree(Expr); IF NOT success THEN Exit;
          (*SEM*) IF IsDecl(identStr) THEN
          Emit1(StoreOpc); (*ENDSEM*)
        END;
      readSy: BEGIN
          NewSy;
          IF SyIsNot(leftParSy) THEN Exit;
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE
            Emit2(ReadOpc, AddrOf(identStr)); (*ENDSEM*)
          NewSy;
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
      writeSy: BEGIN
          NewSy;
          IF SyIsNot(leftParSy) THEN Exit;
          NewSy;
          EmitCodeForExprTree(Expr); IF NOT success THEN Exit;
          (*SEM*) Emit1(WriteOpc); (*ENDSEM*)
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
      beginSy: BEGIN
          NewSy;
          StatSeq; IF NOT success THEN Exit;
          IF SyIsNot(endSy) THEN Exit;
          NewSy;
        END;
      ifSy: BEGIN
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*)IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE BEGIN
            Emit2(LoadValOpc, AddrOf(identStr));
            Emit2(JmpZOpc, 0);
            addr1 := CurAddr - 2;
          END; (*ENDSEM*)
          NewSy;
          IF SyIsNot(thenSy) THEN Exit;
          NewSy;
          Stat; IF NOT success THEN Exit;
          IF sy = elseSy THEN BEGIN
            NewSy;
            (*SEM*) Emit2(JmpOpc, 0);
            FixUp(addr1, CurAddr);
            addr1 := CurAddr -2; (*ENDSEM*)
            Stat; IF NOT success THEN Exit;
            (*SEM*) FixUp(addr1, CurAddr); (*ENDSEM*)
          END;
        END;
      whileSy: BEGIN
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE BEGIN
            addr1 := CurAddr;
            Emit2(LoadValOpc, AddrOf(identStr));
            Emit2(JmpZOpc, 0);
            addr2 := CurAddr - 2;
          END; (*ENDSEM*)
          NewSy;
          IF SyIsNot(doSy) THEN Exit;
          NewSy;
          Stat; IF NOT success THEN Exit;
          (*SEM*) Emit2(JmpOpc, addr1);
          FixUp(addr2, CurAddr); (*ENDSEM*)
        END;
      ELSE
       ; (*EPS*)
    END; (*CASE*)
  END; (*Stat*)

  FUNCTION Expr: TreePtr;
    VAR
      syntaxTree: TreePtr;
      newN: NodePtr;
  BEGIN
    Term(syntaxTree); IF NOT success THEN Exit;
    WHILE (sy = plusSy) OR (sy = minusSy) DO BEGIN
        CASE sy OF
          plusSy: BEGIN
              newN := CreateNode('+', Operation);
              newN^.left := syntaxTree;
              syntaxTree := newN;
              NewSy;
              Term(syntaxTree^.right); IF NOT success THEN Exit;
            END;
          minusSy: BEGIN
              newN := CreateNode('-', Operation);
              newN^.left := syntaxTree;
              syntaxTree := newN;
              NewSy;
              Term(syntaxTree^.right); IF NOT success THEN Exit;
            END;
        END; (*CASE*)
      END; (*WHILE*)
    Expr := syntaxTree;
  END; (*Expr*)

  PROCEDURE Term(VAR syntaxTree: TreePtr);
    VAR newN: NodePtr;
  BEGIN
    Fact(syntaxTree); IF NOT success THEN Exit;
    WHILE (sy = timesSy) OR (sy = divSy) DO BEGIN
        CASE sy OF
          timesSy: BEGIN
              newN := CreateNode('*', Operation);
              newN^.left := syntaxTree;
              syntaxTree := newN;
              NewSy;
              Fact(syntaxTree^.right); IF NOT success THEN Exit;
            END;
          divSy: BEGIN
              newN := CreateNode('/', Operation);
              newN^.left := syntaxTree;
              syntaxTree := newN;
              NewSy;
              Fact(syntaxTree^.right); IF NOT success THEN Exit;
            END;
        END; (*CASE*)
      END; (*WHILE*)
  END; (*Term*)

  PROCEDURE Fact(VAR syntaxTree: TreePtr);
    VAR numberStr: STRING;
  BEGIN
    CASE sy OF
      identSy: BEGIN
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared'); (*ENDSEM*)
          syntaxTree := CreateNode(identStr, Identifier);
          NewSy;
        END;
      numberSy: BEGIN
          Str(numberVal, numberStr);
          syntaxTree := CreateNode(numberStr, Literal);
          NewSy;
        END;
      leftParSy: BEGIN
          NewSy;
          syntaxTree := Expr; IF NOT success THEN Exit;
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
       ELSE
         success := FALSE;
    END; (*CASE*)
  END; (*Fact*)


END. (*MPP_SS*)
