UNIT MPP_SS;

INTERFACE

  VAR
    success: BOOLEAN; (*true if no syntax errros*)

  PROCEDURE S;        (*parses whole MiniPascal program*)


IMPLEMENTATION

  USES
    MP_Lex,
    SymTab,
    CodeGen,
    CodeDef;


  FUNCTION SyIsNot(expectedSy: Symbol): BOOLEAN;
  BEGIN
    success:= success AND (sy = expectedSy);
    SyIsNot := NOT success;
  END; (*SyIsNot*)

  PROCEDURE SemErr(s: String);
  BEGIN
    WriteLn(s);
    Halt;
  END;


  PROCEDURE MP;      FORWARD;
  PROCEDURE VarDecl; FORWARD;
  PROCEDURE StatSeq; FORWARD;
  PROCEDURE Stat;    FORWARD;
  PROCEDURE Expr;    FORWARD;
  PROCEDURE Term;    FORWARD;
  PROCEDURE Fact;    FORWARD;

  PROCEDURE S;
(*-----------------------------------------------------------------*)
  BEGIN
    WriteLn('parsing started ...');
    success := TRUE;
    MP;
    IF NOT success OR SyIsNot(eofSy) THEN
      WriteLn('*** Error in line ', syLnr:0, ', column ', syCnr:0)
    ELSE
      WriteLn('... parsing ended successfully ');
  END; (*S*)

  PROCEDURE MP;
  BEGIN
    (*SEM*) InitSymbolTable; (*ENDSEM*)
    (*SEM*) InitCodeGenerator; (*ENDSEM*)
    IF SyIsNot(programSy) THEN Exit;
    NewSy;
    IF SyIsNot(identSy) THEN Exit;
    NewSy;
    IF SyIsNot(semicolonSy) THEN Exit;
    NewSy;
    IF sy = varSy THEN BEGIN
      VarDecl; IF NOT success THEN Exit;
    END; (*IF*)
    IF SyIsNot(beginSy) THEN Exit;
    NewSy;
    StatSeq; IF NOT success THEN Exit;
    (*SEM*) Emit1(EndOpc); (*ENDSEM*)
    IF SyIsNot(endSy) THEN Exit;
    NewSy;
    IF SyIsNot(periodSy) THEN Exit;
    NewSy;
  END; (*MP*)

  PROCEDURE VarDecl;
  VAR ok: BOOLEAN;
  BEGIN
    IF SyIsNot(varSy) THEN Exit;
    NewSy;
    IF SyIsNot(identSy) THEN Exit;
    DeclVar(identStr, ok);
    NewSy;
    WHILE sy = commaSy DO BEGIN
      NewSy;
      IF SyIsNot(identSy) THEN Exit;
      (*SEM*) DeclVar(identStr, ok); (*ENDSEM*)
      (*SEM*) IF NOT ok THEN
      SemErr('Multiple Declarations'); (*ENDSEM*)
      NewSy;
    END; (*WHILE*)
    IF SyIsNot(colonSy) THEN Exit;
    NewSy;
    IF SyIsNot(integerSy) THEN Exit;
    NewSy;
    IF SyIsNot(semicolonSy) THEN Exit;
    NewSy;
  END; (*VarDecl*)

  PROCEDURE StatSeq;
  BEGIN
    Stat; IF NOT success THEN Exit;
    WHILE sy = semicolonSy DO BEGIN
      NewSy;
      Stat; IF NOT success THEN Exit;
    END; (*WHILE*)
  END; (*StatSeq*)

  PROCEDURE Stat;
  VAR addr1, addr2: INTEGER;
  BEGIN
    CASE sy OF
      identSy: BEGIN
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE
            Emit2(LoadAddrOpc, AddrOf(identStr)); (*ENDSEM*)
          NewSy;
          IF SyIsNot(assignSy) THEN Exit;
          NewSy;
          Expr; IF NOT success THEN Exit;
          (*SEM*) IF IsDecl(identStr) THEN
          Emit1(StoreOpc); (*ENDSEM*)
        END;
      readSy: BEGIN
          NewSy;
          IF SyIsNot(leftParSy) THEN Exit;
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE
            Emit2(ReadOpc, AddrOf(identStr)); (*ENDSEM*)
          NewSy;
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
      writeSy: BEGIN
          NewSy;
          IF SyIsNot(leftParSy) THEN Exit;
          NewSy;
          Expr; IF NOT success THEN Exit;
          (*SEM*) Emit1(WriteOpc); (*ENDSEM*)
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
      beginSy: BEGIN
          NewSy;
          StatSeq; IF NOT success THEN Exit;
          IF SyIsNot(endSy) THEN Exit;
          NewSy;
        END;
      ifSy: BEGIN
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*)IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE BEGIN
            Emit2(LoadValOpc, AddrOf(identStr));
            Emit2(JmpZOpc, 0);
            addr1 := CurAddr - 2;
          END; (*ENDSEM*)
          NewSy;
          IF SyIsNot(thenSy) THEN Exit;
          NewSy;
          Stat; IF NOT success THEN Exit;
          IF sy = elseSy THEN BEGIN
            NewSy;
            (*SEM*) Emit2(JmpOpc, 0);
            FixUp(addr1, CurAddr);
            addr1 := CurAddr -2; (*ENDSEM*)
            Stat; IF NOT success THEN Exit;
            (*SEM*) FixUp(addr1, CurAddr); (*ENDSEM*)
          END;
        END;
      whileSy: BEGIN
          NewSy;
          IF SyIsNot(identSy) THEN Exit;
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE BEGIN
            addr1 := CurAddr;
            Emit2(LoadValOpc, AddrOf(identStr));
            Emit2(JmpZOpc, 0);
            addr2 := CurAddr - 2;
          END; (*ENDSEM*)
          NewSy;
          IF SyIsNot(doSy) THEN Exit;
          NewSy;
          Stat; IF NOT success THEN Exit;
          (*SEM*) Emit2(JmpOpc, addr1);
          FixUp(addr2, CurAddr); (*ENDSEM*)
        END;
      ELSE
       ; (*EPS*)
    END; (*CASE*)
  END; (*Stat*)

  PROCEDURE Expr;
  BEGIN
    Term; IF NOT success THEN Exit;
    WHILE (sy = plusSy) OR (sy = minusSy) DO BEGIN
        CASE sy OF
          plusSy: BEGIN
              NewSy;
              Term; IF NOT success THEN Exit;
              (*SEM*) Emit1(AddOpc); (*ENDSEM*)
            END;
          minusSy: BEGIN
              NewSy;
              Term; IF NOT success THEN Exit;
              (*SEM*) Emit1(SubOpc); (*ENDSEM*)
            END;
        END; (*CASE*)
      END; (*WHILE*)
  END; (*Expr*)

  PROCEDURE Term;
  BEGIN
    Fact; IF NOT success THEN Exit;
    WHILE (sy = timesSy) OR (sy = divSy) DO BEGIN
        CASE sy OF
          timesSy: BEGIN
              NewSy;
              Fact; IF NOT success THEN Exit;
              (*SEM*) Emit1(MulOpc); (*ENDSEM*)
            END;
          divSy: BEGIN
               NewSy;
              Fact; IF NOT success THEN Exit;
              (*SEM*) Emit1(DivOpc); (*ENDSEM*)
            END;
        END; (*CASE*)
      END; (*WHILE*)
  END; (*Term*)

  PROCEDURE Fact;
  BEGIN
    CASE sy OF
      identSy: BEGIN
          (*SEM*) IF NOT IsDecl(identStr) THEN
            SemErr('Variable not declared')
          ELSE
            Emit2(LoadValOpc, AddrOf(identStr)); (*ENDSEM*)
          NewSy;
        END;
      numberSy: BEGIN
          (*SEM*) Emit2(LoadConstOpc, numberVal); (*ENDSEM*)
          NewSy;
        END;
      leftParSy: BEGIN
          NewSy;
          Expr; IF NOT success THEN Exit;
          IF SyIsNot(rightParSy) THEN Exit;
          NewSy;
        END;
       ELSE
         success := FALSE;
    END; (*CASE*)
  END; (*Fact*)


END. (*MPP_SS*)
